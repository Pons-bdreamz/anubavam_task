<?php
// Default Index Controller
class Index extends Controller{
	
	
	public function __construct($controller,$action)
	 {
		parent::__construct($controller, $action); 
		$this->indexmodel=$this->load_model('IndexModel');
		
	//	$this->loadlibrary('captcha/lib','botdetect');
		$this->loadlibrary('image','bootstrap');
		
	 }
	
	public function index()
	{
         $this->view->render('index/index');
	}
	
	public function drawchart()
	{
		$org_path = realpath(__DIR__ . '/../..');
		$image = new Image_Image($org_path.'\img\boat.jpg');
		$image->attach(new Image_Fx_Resize(198));
		$image->attach(new Image_Fx_Crop(196,96));

		
		$wm_image = new Image_Image($org_path.'\img\phpimage.png');
		$wm_image->mid_handle = false;

		$watermark = new Image_Draw_Watermark($wm_image);
		$watermark->setPosition(0,60);

		$image->attach($watermark);
		$image->attach(new Image_Draw_Border(2, "000000"));
		$data = $image->imagePng();
		imagepng($data);
		//$this->view->render('index/index',$data);
	}
	 public function recapt()
	{
		    $ExampleCaptcha = new Captcha("ExampleCaptcha");
			$ExampleCaptcha->UserInputID = "CaptchaCode";
			$res = $ExampleCaptcha->Html();
			$data['cap']=$res;
			$this->view->render('index/index',$data);
	}
	
   	
}
?>