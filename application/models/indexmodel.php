<?php

class IndexModel extends Model 
{ 

	/*
		User Name :  Pons
		User Created on : 06- feb - 2015
		Only User Related Queries
	*/
	public function __construct() 
	{ 
			// Load core model 
			parent::__construct(); 
    }
   
	/*
     * function for getting business and related photo
     * Area : sitebusiness/index,index/index
     */
   public function getbusiness($fields="*",$where=null,$params=array())
   {
	   $query="select $fields from bdz_business b left join bdz_photos p on (b.business_id = p.photo_content_id and p.photo_type = ? and p.photo_deleted = ?) join bdz_url_alias ua on(b.business_id = ua.url_content_id and ua.url_content_type_id = ? and ua.url_alias_active = ? and ua.url_alias_deleted = ?) where 0=0 $where";
	  $qr_stmt=$this->db->prepare($query);
	   $qr_stmt->execute($params);
	   $data=$qr_stmt->FetchAll();
	   return $data;
   }
   
   
    /*
     * function for getting area and city
     * Area : 
     */
   public function getcityarea($fields="*",$where=null,$params=array())
   {
	   $query="select $fields from bdz_cities c left join bdz_area a on (c.city_id = a.city_id and a.area_active =? and a.area_deleted = ?) where 0=0 $where";
	   $qr_stmt=$this->db->prepare($query);
	   $qr_stmt->execute($params);
	   $data=$qr_stmt->FetchAll();
	   return $data;
   }
   
     
    /*
     * function for getting menu's with contenttype and its url_alias_name
     * Area : sitebusiness/index,index/index
     */
   public function getcategoryparentmenu($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_menu m  left join bdz_url_alias u on (m.bdz_menu_url_alias_id = u.url_alias_id and u.url_content_type_id = ? and u.url_alias_active = ? and u.url_alias_deleted = ?) left join bdz_content_type c on(u.url_content_id = c.content_type_id and c.content_type_active = ? and c.content_type_deleted = ?) where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
   
   
    /*
     * function for getting menu's and its url_alias_name 
     * Area : index/index
     */
   public function getcategorymenu($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_menu m  join bdz_url_alias u on (m.bdz_menu_url_alias_id = u.url_alias_id and u.url_alias_active = ? and u.url_alias_deleted = ?) where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
   
   
    /*
     * function for getting city details
     * Area : sitebusiness/index,index/index
     */
   public function getcity($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_cities where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
  
    /*
     * function for getting area details
     * Area : sitebusiness/index,sitebusiness/businesslisting,sitebusiness/taglisting,index/index
     */
   public function getarea($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_area where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
   
    /*
     * function for getting content type details
     * Area : sitebusiness/index,sitebusiness/categorylisting,index/index
     */
    public function getcontenttype($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_content_type where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
   
   /*
     * function for getting category,photo,url_alias_name for given content type id
     * Area : sitebusiness/index,index/index/index
     */
   public function getcategorybycontent($fields="*",$where=null,$params=array())
   {
	   $sql="select $fields from bdz_category c left join bdz_photos p on(c.category_photo_id = p.photo_id and p.photo_type = ? and p.photo_content_type_id = ? and p.photo_deleted = ?)join bdz_url_alias u on (c.category_id = u.url_content_id and u.url_content_type_id = ? and u.url_alias_active = ? and u.url_alias_deleted = ?) where 0=0 $where";
	   $sql_stmt=$this->db->prepare($sql);
	   $sql_stmt->execute($params);
	   $res=$sql_stmt->FetchAll();
	   return $res;
   }
   
  
   
} 
?>