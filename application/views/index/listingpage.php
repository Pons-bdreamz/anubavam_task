<div class="row rowcontainer">
<div class="container new_con mg-bottom">
	<div class="col-lg-4">
          <img alt="W3" border="0" src="new admin.png" width="69%"/>
	</div>
</div>
<div class="container new_con height">
<div class="">

<div class="col-lg-3">
<h4>List of PHP Training in chennai</h4>
<div id="accordian">
	<ul>
	<li>
	<div class="filter">
FILTER BY:
	<a class="pull-right" href="listing.php">RESET</a>
</div>
</li>
<?php 
foreach($filter_tag as $tag) { ?>
		<li>
			<h3><?php echo $tag['filter_type_name']; ?><span class="glyphicon glyphicon-menu-down  new_down pull-right"></h3>
			<ul>
				<?php foreach($tag['filtertags'] as $subtag) {  ?>
					<li><input type="checkbox" class="category_filter_tags" value="<?php echo $subtag['filter_tag_id']; ?>"> <?php echo $subtag['filter_tag_name']; ?></li>
				<?php } ?>
			</ul>
		</li>
<?php } ?>	
	</ul>
</div>

</div>
<input type="hidden" name="page_category" id="page_category" value="<?php echo $category_id; ?>">

<div class="col-lg-9" id="category_page_details">
<?php 
for($i=0;$i<count($business);$i++)
{?>
<div class="m-top">
		<div class="rt-header ">
			<h3><a href="<?php echo BASE_URL."/".$business[$i]['business_url'] ;?>"><?php echo ucfirst($business[$i]['business_name']).','.$business[$i]['area_name'].'.';?></a></h3>
			<span class="star-rating ">
				<i class="fa fa-star scolor"></i>
				<i class="fa fa-star scolor"></i>
				<i class="fa fa-star scolor"></i>
				<i class="fa fa-star scolor"></i>
				<i class="fa fa-star scolor"></i>
			</span>
			<div class="logo-right pull-right">
				   <img src="<?php echo USER_IMAGES.$business[$i]['photo_path'];?>"  alt="www.besant.in"></img>
			</div>
			
		</div>
		<div class="row">
			<div class="col-lg-7">
				<div class="tag">
					<i class="glyphicon glyphicon-tag"></i>
					<em><?php echo $business[$i]['tag_name'] ;?></em>
				</div>
				<div class="location">
					<i class="glyphicon glyphicon-map-marker"></i>
					<em><?php echo $business[$i]['area_name'].',';?></em>
					<em><?php echo $business[$i]['city_name'].'.';?></em>
					
				</div>
			</div>
			<div class="col-lg-5">
				<div class="row">
					<div class="col-lg-12 ">
					<a href="../frontend/mappingpage"><button class="btn bg-maroon btn-flat margin button-css pull-right" type="button"><i class="glyphicon glyphicon-map-marker glyphicon-circle backgorund"></i>Direction</button></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ">
					<div class="dropdown pull-right">
					  <button class="btn dropdown-toggle newbutton" type="button" data-toggle="dropdown">
					  <i class="glyphicon glyphicon-earphone"></i>Phone numbers
					  <span class="caret"></span></button>
					  <ul class="dropdown-menu">
						<?php if(!empty($business[$i]['business_contact'])){?><li><a href="#"><?php echo $business[$i]['business_contact'];?></a></li><?php }?>
						<?php if(!empty($business[$i]['business_phone'])){?><li><a href="#"><?php echo $business[$i]['business_phone'];?></a></li><?php }?>
					  </ul>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="details ">
			<div class="row">
				<div class="col-lg-12">
					<i class="glyphicon glyphicon-user"></i>
					<em>About</em>
				</div>
			</div>
				<div class="cdesc new_cdesc" id="more_description"><?php echo $business[$i]['business_details'];?></div>
				<a href="#" class="view_more" style="display: inline-block">View More</a>
		</div>
</div>
<?php }?>
</div> 
<div class="text-center simple_paginate">
<ul class="pagination">
  <li><a href="#">«</a></li>
  <li><a href="#">1</a></li>
  <li><a class="active" href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">»</a></li>
</ul>
</div>
<!--   <div id="pagination-demo1"></div>-->

</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$(".filter-header").on('click',function(){
		$(".filter_sub").slideToggle("500");
	});

	$(".filter-down").on('click',function(){
	$(".filter_add").slideToggle("slow");
	});

	/*view more dropdown*/

	$('.view_more').on('click',function(e){
		$(this).prev().removeClass('cdesc');
		//$('more_description').addClass('moredesc');
		$(this).hide();
		});
		
	$( ".new_cdesc" ).each(function( index ) {
	  var desclen = $( this ).text();
	  if(desclen.length <= 153) 
		{
			$( this ).next().hide();
		}
	});
	
/*accordian*/	
	$("#accordian h3").click(function(){
		//slide up all the link lists
		$("#accordian ul ul").slideUp();
		//slide down the link list below the h3 clicked - only if its closed
		if(!$(this).next().is(":visible"))
		{
			$(this).next().slideDown();
		}
	});
});

 function filterlist(){
	
	if($("input.category_filter_tags:checked").length>0)	 
	 {
		 var filters = new Array();
		$.each($("input.category_filter_tags:checked"),function(e){
			filters.push($(this).val());
		});
		var page_category_id = $("#page_category").val();
		$.ajax({
			type:'post',
			data:{
				'filters':filters,
				'filter_category':page_category_id
			},
			url:'/frontend/filterbusinesslist',
			success:function(response)
			{
				var result = eval('('+response+')');
				if(result.success == true)
				{
					$("#category_page_details").empty();
					$("#category_page_details").html(result.data);
				}
				else
				{
					$("#category_page_details").empty();
					$("#category_page_details").html('<div>No Records Found</div>');
				}
				
			}
		});
	 }
	 else
	 {
		 var page_category_id = $("#page_category").val();
		 $.ajax({
			type:'post',
			data:{
				'filter_category':page_category_id
			},
			url:'/frontend/filterbusinesslist',
			success:function(response)
			{
				var result = eval('('+response+')');
				if(result.success == true)
				{
					$("#category_page_details").empty();
					$("#category_page_details").html(result.data);
				}
				else
				{
					$("#category_page_details").empty();
					$("#category_page_details").html('<div>No Records Found</div>');
				}
				
			}
		});
	 }
	
 }



$(".category_filter_tags").on('change',function(){
	filterlist();
});

$("#simple_paginate").pagination({
        items: 100,
        itemsOnPage: 10,
        cssStyle: 'light-theme'
    });

    </script>