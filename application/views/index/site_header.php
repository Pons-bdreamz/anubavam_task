<!DOCTYPE html>
<html>
<head>
    
                <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, initial-scale=1.0"/>
                <meta name="robots" content="NOODP,NOYDIR"/>
		<title><?php echo (isset($browser_title)&& !empty($browser_title))?$browser_title:BDZ_DEF_TITLE; ?></title>
		<meta name="description" content="<?php echo (isset($browser_desc)&& !empty($browser_desc))?$browser_desc:BDZ_DEF_TITLE; ?>"/>
		<meta name="author" content=""/>
		<link rel="canonical" href="<?php echo $current_url; ?>"/>
		<meta property="fb:admins" content=""/>
		<meta property="fb:app_id" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:url" content="<?php echo $current_url; ?>"/>
		<meta property="og:title" content="<?php echo (isset($browser_title)&& !empty($browser_title))?$browser_title:BDZ_DEF_TITLE; ?>"/>
		<meta property="og:description" content="<?php echo (isset($browser_desc)&& !empty($browser_desc))?$browser_desc:BDZ_DEF_TITLE; ?>"/>
		<meta property="og:image" content=""/>
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:url" content="<?php echo $current_url; ?>"/>
		<meta name="twitter:site" content=""/>
		<meta name="twitter:title" content="<?php echo (isset($browser_title)&& !empty($browser_title))?$browser_title:BDZ_DEF_TITLE; ?>"/>
		<meta name="twitter:description" content="<?php echo (isset($browser_desc)&& !empty($browser_desc))?$browser_desc:BDZ_DEF_TITLE; ?>"/>
		<meta name="twitter:image" content=""/>

		<link rel="icon" type="image/png" href="http://www.xyz.com/favicon.png"/>
		<link href="https://plus.google.com/xyz" rel="publisher"/>
		<link type="application/opensearchdescription+xml" rel="search" href="http://www.xyz.com/opensearch.xml"/>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" type="text/css" rel="stylesheet"/>
		
                <link rel="stylesheet" href="<?php echo SITE_THEME."/css/site_theme.css"; ?>">
                <link rel="stylesheet" href="<?php echo SITE_THEME."/css/site_template.css"; ?>">
                <link rel="stylesheet" href="<?php echo SITE_THEME."/css/sitte_theme.css"; ?>">
                <script src="<?php echo SITE_THEME."/js/site_header.js"; ?>"></script>
				
 
</head>
 

<body class="skin-blue sidebar-mini">
 <nav class="navbar navbar-default">
        <div class="container">

	 <ul class="nav navbar-nav ">
          <li class="active"><a href="">Home</a></li>
	
      </ul>
	 

            <?php if(!empty($_SESSION['loged_user_id'])) { ?>
              <div class="text-right" style="margin:10px;"><a href="/user">Settings</a></div>
            <?php } else { ?>
                <div class="text-right" style="margin:10px;"><a href="#designmodel" class="" data-target="#usermodel" data-toggle="modal">Login</a></div>
            <?php } ?>
        </div>
    
</nav>