<!--LoginModal Starts -->
<div id="usermodel" class="modal fade" role="dialog" tabindex="-1" style="display: none;">
	<div class="modal-dialog modal-sm clearfix">
		<!-- Modal content-->
		<div class="modal-content onecolumn clearfix">
			<div class="loginModalform formblock col-sm-12 col-xs-12">
				<div class="modal-header">
					<h4 class="modal-title tac">Sign up or log in to Flikza</h4>
				</div>
				<div class="modal-body">
					<div class="loginhome" id="loginhome">
						<div class="sociallogin">
							<!-- <div class="fblogin" id="fb_btn">
								<a href="">
									<i class="fa fa-facebook-square" aria-hidden="true"></i>
									<span>Login using Facebook</span>
								</a>
							</div> -->
						<div class="googlelogin" id="customBtn">
							<a href="#">
								<i class="fa fa-google-plus-square" aria-hidden="true"></i>
								<span>Login using Google</span>
							</a>
						</div>
						<div class="twitterlogin">
							<a href="/user/twitterlogin">
								<i class="fa fa-twitter-square" aria-hidden="true"></i>
								<span>Login using Twitter</span>
							</a>
						</div>
					</div>
					<div class="hrrule"><span>OR</span><hr/></div>
						<div class="nonsociallogin">
							<div class="tac mb10 fsize16">Or use your Email address</div>
							<div class="row mb10">
								<div class="col-lg-6"><a href="" class="btn btn-danger btn-in btn-block incpadding"><span>Log in</span></a></div>
								<div class="col-lg-6"><a href="" class="btn btn-default btn-up btn-block incpadding"><span>Sign up</span></a></div>
							</div>
							<div class="disclaimer tac fsize12">By logging in, you agree to Flikza's <a href="/terms-conditions">Terms of Service</a>, <a href="/cookie-policy">Cookie Policy</a>, <a href="/privacy-policy">Privacy Policy</a> and <a href="/terms-of-use">Content Policies</a>.</div>
						</div>
					</div>
					<!-- Login Form Begins -->
					<div class="loginprim" id="loginprim">
						<form id="user_login" name="user_login" method="post" data-url="/user/signin">
							<div id="agree" class="hide">
								<span class="status-msg">Please ensure all fields are valid.</span>
							</div>
							<div class="form-group">
								<span class="labelstyle displayblock clearfix fsize16">Email</span>
								<div class="icon-addon addon-lg">
									<input type="text" placeholder="Email *" id="inputusermail" class="form-control" name="inputusermail" valid-name="email"/>
									<label for="loginPrimarymail" class="glyphicon glyphicon-envelope" rel="tooltip" title="Email"></label>
									<input type="hidden" name="token" id="token" value="<?php echo $_SESSION['csrftoken']; ?>">
								</div>
							</div>
							<div class="form-group">
								<span class="labelstyle displayblock clearfix fsize16">Password</span>
								<div class="icon-addon addon-lg">
									<input type="password" placeholder="Password *" id="inputuserpwd" class="form-control" name="inputuserpwd" valid-name="password"/>
									<label for="loginPrimarypwd" class="glyphicon glyphicon-lock" rel="tooltip" title="Password"></label>
								</div>
							</div>
							<div class="clearfix">
								<!-- <div class="pull-left">
									<input type="checkbox" name="rememberchk" id="rememberchk" value="true" checked="true" >
									<label for="rememberchk" class="rememberchklabel loginmdlchklabel">Remember me</label>
								</div> -->
								<div class="pull-right">
									<a href="#" class="btn-fpwd">Forget Password?</a>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-danger btn-block signinbutton modalbtn user-submit">Log In</button>
							</div>   
						</form>
						<div class="disclaimer tac fsize12">By logging in, you agree to Flikza's <a href="/terms-and-conditions">Terms of Service</a>, <a href="/cookie-policy">Cookie Policy</a>, <a href="/privacy-policy">Privacy Policy</a> and <a href="/terms-of-use">Content Policies</a>.</div>
					</div>
					<!-- Login Form Ends -->
					<!-- Signup Form Begins -->
					<div class="signupprim" id="signupprim">
						<form id="user_signup" name="user_signup" method="post" data-url="/user/signup">
							<div id="agree" class="hide">
								<span class="status-msg">Please ensure all fields are valid.</span>
							</div>
							<div class="form-group">
								<span class="labelstyle displayblock clearfix fsize16">Full Name</span>
								<div class="icon-addon addon-lg">
									<input type="text" placeholder="Full Name *" id="username" class="form-control" name="username" valid-name="fullname"/>
									<label for="signupPrimaryfname" class="glyphicon glyphicon-user" rel="tooltip" title="Full Name"></label>
								</div>
							</div>
							<div class="form-group">
								<span class="labelstyle displayblock clearfix fsize16">Email</span>
								<div class="icon-addon addon-lg">
									<input type="text" placeholder="Email *" id="useremail" class="form-control" name="useremail" valid-name="email"/>
									<label for="signupPrimarymail" class="glyphicon glyphicon-envelope" rel="tooltip" title="Email"></label>
									<input type="hidden" name="token" id="token" value="<?php echo $_SESSION['csrftoken']; ?>">
								</div>
							</div>
							<div class="form-group">
								<span class="labelstyle displayblock clearfix fsize16">Password</span>
								<div class="icon-addon addon-lg">
									<input placeholder="Password *" id="pwd" class="form-control" name="userpwd" valid-name="password" type="password"/>
									<label for="signupPrimarypwd" class="glyphicon glyphicon-lock" rel="tooltip" title="Password"></label>
								</div>
							</div>
							<div class="clearfix">
								<div class="pull-left">
									<input type="checkbox" name="signupchk" id="signupchk" class="form-control" value="true" checked="true">
									<label for="newsltrsubscri" class="newsltrsubscrilabel loginmdlchklabel">Send me occasional email updates</label>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-danger btn-block signupbutton modalbtn user-submit">Sign Up</button>
							</div>
						</form>
						<div class="disclaimer tac fsize12">By creating an account, you agree to Flikza's <a href="#">Terms of Service</a>, <a href="#">Cookie Policy</a>, <a href="#">Privacy Policy</a> and <a href="#">Content Policies</a>.</div>
					</div>
					<!-- Signup Form Ends -->
					<!-- Forget Password Begins -->
					<div class="forgetpwdprim" id="forgetpwdprim">
						<span class="tac mb10">Please enter the email address you signed up with and we'll send you a password reset link.</span>
						<form id="user_frgtpwd" name="user_frgtpwd" method="post" data-url="/user/forgetpwd">
							<div id="agree" class="hide">
								<span class="status-msg">Please ensure all fields are valid.</span>
							</div>
							<div class="form-group">
								<div class="icon-addon addon-lg">
									<input type="text" placeholder="Email *" id="usermailid" class="form-control" name="usermailid" valid-name="email"/>
									<label for="forgetpwdPrimarymail" class="glyphicon glyphicon-envelope" rel="tooltip" title="Email"></label>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-danger btn-block signupbutton modalbtn user-submit">Reset Password</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<button type="button" class="close loginModalclosebtn cusmodalclosebtn" data-dismiss="modal">&times;</button>
		</div>
	</div>
</div>
<!--LoginModal Ends -->