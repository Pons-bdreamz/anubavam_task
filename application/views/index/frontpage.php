<div id="sticky-header" class="">
			<header id="headerid" class="headerclass">
				<div class="secmenu hidden-xs">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 secmenuleft">
								<ul class="pull-left list-unstyled nopaddmari">
									<li class="pull-left"><a href="#" title="Education" class="nav-link active">Education</a></li>
									<li class="hide"><a href="#" title="Health &amp; Wellness" class="nav-link">Health &amp; Wellness</a></li>
								</ul>
							</div>
							
							<div class="col-sm-6 secmenuright">
								<ul class="pull-right list-unstyled nopaddmari">
									<li class="pull-right"><a href="#" title="Add Business" class="nav-link">Add Business</a>
									
									
									</li>
									<li class=""><a href="#" title="Help" class="nav-link">Help</a></li>
									<li class=""><a href="#" title="Download App" class="nav-link">Download App</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="primary">
					<div class="container">
						<div class="row">
							<div class="col-sm-2 primarylogo">
								<div class="logocontainer pull-left">
									<a href="#" title="Logo" class="logo">
										<img src="assets/images/dummy-logo-2.png" title="Logo" alt="XYZ" class=""/>
									</a>
								</div>
							</div>
							<div class="col-sm-10">
								<div class="primitems clearfix">
									<div class="locselec pull-left">
										<a title="" id="currlocation">
											<i class="fa fa-map-marker" aria-hidden="true"></i>
											<span id="currloc">Chennai</span>
										</a>
										<div class="dropdownwrapper" id="locupdate" style="display:none;">
											<div class="dropdowncontent">
												<div class="locsearchbox formele">
													<form>
														<input type="text" name="locsearch" id="locsearchid" class="locsearchclass form-input form-control" placeholder="Enter Your City">
													</form>
												</div>
												<div class="locsugg">
													<!--<span>Top Searched</span>-->
													<div class="locsuggcontent">
														<a href="#" title="">Chennai</a>
														<a href="#" title="">Bangalore / Bengaluru</a>
														<a href="#" title="">National Capital Region (NCR)</a>
														<a href="#" title="">Mumbai</a>
														<a href="#" title="">Pune</a>
														<a href="#" title="">Hyderabad</a>
														<a href="#" title="">Kolkata</a>
													</div>
													<a href="#" class="locallcities">All Cities</a>
												</div>
											</div>
										</div>
									</div>
									<div class="searchcontainer pull-left" id="searchid">
										<form class="" action="" method="get">
											<div class="searchinput formele">
												<input type="text" lang="en" x-webkit-grammar="builtin:translate" x-webkit-speech="x-webkit-speech" autocomplete="off" name="SearchText" class="ui-searchbar-keyword form-input form-control" placeholder="What you are looking for today..." />
												<button id="" type="submit" class="btn btn-warning"><i aria-hidden="true" class="fa fa-search"></i> <span>Search</span></button>
											</div>
										</form>
									</div>
									<div class="accountssec pull-right">
										<a title="" id="acctextgroup">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span id="acctext">Login / Register</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
		</div>
		<div class="mainwrapper" id="contentwrapper">
			<div class="breadcrumbcont">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="mainblock bcblock">
								<ol class="nopaddmari">
									<li><a href="#">Home</a></li>
									<li><a href="#">India</a></li>
									<li><a href="#">Tamil Nadu</a></li>
									<li><a href="#">Chennai</a></li>
									<li><a href="#">Velachery</a></li>
									<li><a href="#">Besant Technologies</a></li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="buss-header-container">
				<div class="buss-header">
					<div class="buss-map-image hide">
						<div class="map-container">
							<div class="mappin"></div>
							<img src="assets/images/sample-google-map.jpg" class="staticmap" title="Besant Technologies in Google Maps"/>
							<a href="#" title="" class="pull-right btn btn-default btn-sm mapexpand" id="">Expand Map / Find Directon</a>
						</div>
					</div>
					<div class="buss-dummy-bg"></div>
				</div>
				<div class="buss-info-container">
					<div class="container">
						<div class="buss-pri-info-group">
							<div class="buss-logo-group">
								<div class="buss-logo">
									<a href="#" title="" class="">
										<img src="assets/images/besant-technologies-logo-timestamp.jpg" class="buss-logo-image" title="Besant Technologies">
									</a>
								</div>
							</div>
							<div class="buss-pri-info">
								<div class="buss-name">
									<h1 class="buss-title">
										<span class="buss-title-name" itemprop="name">Besant Technologies</span>
									</h1>
								</div>
								<div class="buss-pri-links pull-left">
									<div class="buss-links-group">
										<ul class="buss-pri-ul">
											<li class="buss-pri-ul-li">
												<a href="#" title="" rel="" class="active">
													<span>Overview</span>
												</a>
											</li>
											<li class="buss-pri-ul-li">
												<a href="#" title="" rel="" class="">
													<span>Reviews (342)</span>
												</a>
											</li>
											<li class="buss-pri-ul-li">
												<a href="#" title="" rel="" class="">
													<span>Photos (3)</span>
												</a>
											</li>
											<li class="buss-pri-ul-li">
												<a href="#" title="" rel="" class="">
													<span>Send Enquiry</span>
												</a>
											</li>
											<li class="buss-pri-ul-li">
												<a href="#" title="" rel="" class="">
													<span>Get Quote</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="buss-reviews-pri-block pull-right">
									<div class="buss-reviews-stars pull-left">
										<i aria-hidden="true" class="fa fa-star"></i>
										<i aria-hidden="true" class="fa fa-star"></i>
										<i aria-hidden="true" class="fa fa-star"></i>
										<i aria-hidden="true" class="fa fa-star"></i>
										<i aria-hidden="true" class="fa fa-star"></i>
									</div>
									<div class="buss-reviews-count pull-left"><a href="#" title="Besant Technologies Reviews">342 Reviews</a></div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="buss-data-container">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="mainblock buss-data-block clearfix">
								<div class="buss-profile-block col-sm-6">
									<ul class="nopaddmari">
										<li>
											<i class="fa fa-phone pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Phone No</strong>
											<div class="buss-phone">
												<span class="pull-left">+91 44 33055404</span>
											</div>
										</li>
										<li>
											<i class="fa fa-mobile pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Mobile No</strong>
											<div class="buss-mobile">
												<span class="pull-left">+91 9988776568</span>
												<span class="pull-left">/</span>
												<span class="pull-left">+91 9988776568</span>
											</div>
										</li>
										<li>
											<i class="fa fa-location-arrow pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Address</strong>
											<div class="buss-address-block">
												<span class="pull-left">No. 24, Nagendra Nagar, Velachery Main Road, </span>
												<span class="pull-left"><a href="#">Velachery</a>, </span>
												<span class="pull-left"><a href="#">Chennai</a> - </span>
												<span class="pull-left">600 042</span>
												<span class="pull-left">&nbsp;&nbsp;[&nbsp;<a href="#" title="Get Directions to Besant Technologies" class="">Get Directions</a>&nbsp;]</span>
											</div>
										</li>
										<li>
											<i class="fa fa-map-marker pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Landmark</strong>
											<div class="buss-landmark-block">
												<span class="pull-left">Opposite to Phoenix Market City &amp; Next to Ramkay TVS</span>
											</div>
										</li>
										<li>
											<i class="fa fa-envelope pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Email</strong>
											<div class="buss-email">
												<span class="pull-left"><a href="#" title="">test@besanttech.com</a></span>
											</div>
										</li>
										<li>
											<i class="fa fa-globe pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Website</strong>
											<div class="buss-web">
												<span class="pull-left"><a href="#">www.besanttechnologies.com</a></span>
											</div>
										</li>
										<li>
											<i class="fa fa-user pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Contact Person</strong>
											<div class="buss-cont-person">
												<span class="pull-left">Vijay Srinivasan</span>
											</div>
										</li>
									</ul>
								</div>

								<div class="buss-profile-block col-sm-6">
									<ul class="nopaddmari">
										<li>
											<i class="fa fa-play pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Established</strong>
											<div class="buss-estab">
												<span class="pull-left">2004</span>
											</div>
										</li>
										<li>
											<i class="fa fa-clock-o pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Operating Hours</strong>
											<div class="buss-open-hours">
												<span class="pull-left hide">Wednesday  12 Noon to 3:30 PM, 7 PM to 10:30 PM</span>
												<span class="pull-left">Wednesday  7 AM to 10:30 PM</span>
												<a href="#" title="" class="">see more</a>
											</div>
										</li>
										<li>
											<i class="fa fa-users pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Employees</strong>
											<div class="buss-emp-count">
												<span class="pull-left">20</span>
											</div>
										</li>
										<li>
											<i class="fa fa-money pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Payments</strong>
											<div class="buss-payment-accept">
												<span class="pull-left">Cash,</span><span class="pull-left">Master Card,</span><span class="pull-left">Visa Card,</span><span class="pull-left">Debit Cards,</span><span class="pull-left">Cheques,</span><span class="pull-left">American Express Card</span>
											</div>
										</li>
										<li>
											<i class="fa fa-tag pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Category</strong>
											<div class="buss-pri-categ">
												<span class="pull-left"><a href="#" title="">Education &amp; Training</a></span>
											</div>
										</li>
										<li>
											<i class="fa fa-tags pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Sub Category</strong>
											<div class="buss-sub-categ">
												<span class="pull-left"><a href="#" title="">IT Training</a>,</span><span class="pull-left"><a href="#" title="">Non IT Training</a>,</span><span class="pull-left"><a href="#" title="">Classroom Training</a>,</span><span class="pull-left"><a href="#" title="">Online Training</a>,</span><span class="pull-left"><a href="#" title="">Corporate Training</a>,</span><span class="pull-left"><a href="#" title="">E-Learning</a></span>
											</div>
										</li>
										<li>
											<i class="fa fa-share-alt pull-left" aria-hidden="true"></i>
											<strong class="pull-left">Social</strong>
											<div class="buss-social-icons">
												<span class="pull-left"><a href="#" title=""><i class="fa fa-facebook-square nopaddmari" aria-hidden="true"></i> Facebook</a></span><span class="pull-left"><a href="#" title=""><i class="fa fa-twitter-square nopaddmari" aria-hidden="true"></i> Twitter</a></span><span class="pull-left"><a href="#" title=""><i class="fa fa-linkedin-square nopaddmari" aria-hidden="true"></i> LinkedIn</a></span><span class="pull-left"><a href="#" title=""><i class="fa fa-google-plus-square nopaddmari" aria-hidden="true"></i> Google Plus</a></span>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="buss-more-details text-right clearfix">
								<div class="pull-left">
									<a href="#" title="" class="btn btn-sm btn-primary"><i class="fa fa-bookmark-o" aria-hidden="true"></i><span>Send to Phone / Email</span></a>
								</div>
								<div class="pull-left btn-shotlist">
									<a href="#" title="" class="btn btn-sm btn-success"><i class="fa fa-heart-o" aria-hidden="true"></i><span>Shotlist</span></a>
								</div>
								<div class="pull-left btn-print">
									<a href="#" title="" class="btn btn-sm btn-default"><i class="fa fa-print" aria-hidden="true"></i><span>Print</span></a>
								</div>
								<div class="pull-right">
									<a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-exclamation-circle" aria-hidden="true"></i><span>Report Error</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="buss-about-container">
				<div class="container">
					<div class="row">
						<div class="">
							<div class="buss-data-block buss-about-block clearfix">
								<div class="buss-about-text-block col-sm-8">
									<div class="mainblock">
									<h3>About Besant Technologies</h3>
									<p>Besant Technologies, was founded by professionals from the academia and industry, with a vision to shape young minds into competent technical professionals. Besant Technologies aims to address the growing demand for quality and trained talent by providing focused, relevant and customized learning opportunities combined with hand-on experience on live project environment.</p>
									<p>With expertise across a wide range of technologies including ERP, Databases, Programing Languages, etc. Besant is an ideal choice for professionals and enterprises seeking to enhance their or their employee’s technical skills and remain relevant in the market place.</p>
									<p>Besant, is committed to providing their students with an ideal learning environment and hence has set up a state of the art learning laboratory, which is accessible to both current and alumni students on round the clock service. </p>
									<p>Besant is uniquely place to meet the training requirements of the industry for its team of trainers and technical experts are actively engaged with leading technology players and are sought after for their technical expertise by the industry.</p>
									<p>Besant, is the preferred learning partner for professionals and enterprises seeking customized training on current and legacy technologies and domains. Besant's methodology of designing the course curriculum based on the need-gap assessment done by a panel of technical experts ensures an optimum roIe and guarantees tangible</p>
									</div>
									<div class="mainblock">
											<h3>Also list courses </h3>
											<div class="category-list">
											<div class="row">
												<div class="col-lg-12 mar-bottom">
											<i class="fa fa-tag pull-left" aria-hidden="true"></i>
										<!--	<span class="mar-name"><img src="assets/images/tag1.png" width="2%">Web Designing</span>-->
													<strong class="strong-size">Web Designing</strong>
												</div>
												<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
													<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true" style="color:white"></i></span>
													<span class="">Advance Design</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Advance Designing</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Website Scripting</span>-->
														<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Website script</span>
													</div>
													<div class="sub-category-list col-lg-3">
												<!--	<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">HTML</span>-->
														<span class="sub-tag"><img src="assets/images/tag.png" width="12%">HTML</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">ColdFusion</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">PHP</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true" style="color:white"></i></span>
													<span class="">Dynamic Websites</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">ColdFusion</span>
													</div>
													<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">HTML5</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">jquery</span>
													</div>
													<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">PhotoShop</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">javascript</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
												</div>
												
												<div class="col-lg-12 mar-bottom">
												<i class="fa fa-tag pull-left" aria-hidden="true"></i>
													<strong class="strong-size">Software Testing
</strong>
												</div>
												<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left font-weight" aria-hidden="true"></i></span>
													<span class="">Manual Testing</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Mercury</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
												</div>
												<div class="col-lg-12 mar-bottom">
												<i class="fa fa-tag pull-left" aria-hidden="true"></i>
													<strong class="strong-size">Networking
</strong>
												</div>
													<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Manual Testing</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Manual Testing</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Mercury</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Mercury</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Perl CGI</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Perl CGI</span>
													</div>
												</div>
													<div class="col-lg-12 mar-bottom">
												<i class="fa fa-tag pull-left" aria-hidden="true"></i>
													<strong class="strong-size">Big Data
</strong>
												</div>
														<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Manual Testing</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Manual Testing</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Mercury</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Mercury</span>
													</div>
														<div class="sub-category-list col-lg-3">
														<!--<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Perl CGI</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
															<div class="sub-category-list col-lg-3">
													<!--	<span class="category-icon">
													<i class="fa fa-tags pull-left" aria-hidden="true"></i></span>
													<span class="">Perl CGI</span>-->
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Perl CGI</span>
													</div>
													
												</div>
												<div class="col-lg-12 mar-bottom">
												<i class="fa fa-tag pull-left" aria-hidden="true"></i>
													<strong class="strong-size">Oracle Application
</strong>
												</div>
												<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
															<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
												</div>
												<div class="col-lg-12 mar-bottom">
												<i class="fa fa-tag pull-left" aria-hidden="true"></i>
													<strong class="strong-size">Oracle Application
</strong>
												</div>
												<div class="col-lg-12 tag-border">
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
														<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
															<div class="sub-category-list col-lg-3">
													<span class="sub-tag"><img src="assets/images/tag.png" width="12%">Design HTML5</span>
													</div>
													
													

													</div>
													
													
													
													
											</div>
										
											
									</div>
								</div>
								</div>
								<div class="buss-others-block col-sm-4">
									<div class="mainblock">
										<h3>Photos</h3>	
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant2.png" alt="">
													</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant2.png" alt="">
													</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant1.png" alt="">
													</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant2.png" alt="">
													</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant1.png" alt="">
													</a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 thumb">
													<a class="thumbnail" href="#">
														<img class="img-responsive" src="assets/images/besant2.png" alt="">
													</a>
											</div>
										</div>
									
									</div>
								</div>
								
								<div class="mainblock">
									<h3>Reviews</h3>
									<div class="row review-rating-star">
										
										<div class="col-sm-4">
											<span class="rating-count">15</span>
											<div class="rating">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
												<span class="reviews-rating">455 Reviews </span>
											</div>
										</div>
										
										
										<div class="col-sm-8">
											<div class="rating-bar-container">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>5
												</span>
											
											
											<div class="progress pull-right ">
												    <div class="progress-bar " role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:95%">
														<span class="sr-only">82% Complete</span>
													</div>
												</div>
											</div>
											<div class="rating-bar-container">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>4
												</span>
											
												
											<div class="progress pull-right">
												    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:75%">
														<span class="sr-only">82% Complete</span>
													</div>
												</div>
											</div>
											<div class="rating-bar-container">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>3
												</span>
											
												
											<div class="progress pull-right">
												    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">
														<span class="sr-only">82% Complete</span>
													</div>
												</div>
											</div>
											<div class="rating-bar-container">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>2
												</span>
											
												
											<div class="progress pull-right">
												    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:40%">
														<span class="sr-only">82% Complete</span>
													</div>
												</div>
											</div>
												<div class="rating-bar-container">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>1
												</span>
											
											<div class="progress pull-right">
												    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:30%">
														<span class="sr-only">82% Complete</span>
													</div>
												</div>
											</div>
											
										</div>
										</div>
										<div class="review-write-read">
											<div class="read-write">
											<a href="" class="read-tag">Read Reviews</a>
											<a href="" class="write-tag pull-right">write reviews</a>
											</div>
											<div class="">
												<div class="written-review">
												
												<div class="review-table">
													
													<div class="review-user_image col-lg-2">
														<img alt="Sashikumar P" width="75px" src="assets/images/he.gif" class="" onerror="replaceMissingImages(this, 'avatar');">
													</div>
													<div class="col-lg-10 review-user_name">
														<span class="lable-name">prashanth</span>
														<span class="date">9
														<sup>th</sup>March,2016
														</span>
													<br>
														<span class="star-rating star-light">
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
														</span>
														</div>
													</div>
												
													<p>I have undergone PEGA training in Besant technologies. My trainer name is Karthick. He gave such a good training in both technical and theoretical. Now i have cleared my CSA certification and Joined a Company. </p>
												</div>
												<div class="written-review">
												
												<div class="review-table">
													
													<div class="review-user_image col-lg-2">
														<img alt="Sashikumar P" width="75px" src="assets/images/he.gif" class="" onerror="replaceMissingImages(this, 'avatar');">
													</div>
													<div class="col-lg-10 review-user_name">
														<span class="lable-name">Ravikumar</span>
														<span class="date">9
														<sup>th</sup>March,2016
														</span>
													</br>
														<span class="star-rating star-light">
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
														</span>
														</div>
													</div>
												
													<p>I have undergone PEGA training in Besant technologies. My trainer name is Karthick. He gave such a good training in both technical and theoretical. Now i have cleared my CSA certification and Joined a Company. </p>
												</div>

											
												
											<div class="written-review">
												
												<div class="review-table">
													
													<div class="review-user_image col-lg-2">
														<img alt="Sashikumar P" width="75px" src="assets/images/he.gif" class="" onerror="replaceMissingImages(this, 'avatar');">
													</div>
													<div class="col-lg-10 review-user_name">
														<span class="lable-name">praveen</span>
														<span class="date">9
														<sup>th</sup>March,2016
														</span>
													<br>
														<span class="star-rating star-light">
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
															<i class="fa fa-star scolor"></i>
														</span>
														</div>
													</div>
												
													<p>I have undergone PEGA training in Besant technologies. My trainer name is Karthick. He gave such a good training in both technical and theoretical. Now i have cleared my CSA certification and Joined a Company. </p>
												</div>
												</div>
												<div class="write-review">
												</div>
										</div>
										
									
								
								</div>
								</div>
							
						
							</div>
							
						</div>
						
					</div>
					
						<div class="clearfix"></div>
							<div class="mainblock">
								<h3>Find other Administration Training in Velachery</h3>
								<div class="row">
								<div class="col-lg-12 mar-bottom">
								
									<div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Kernel Zone Solution</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
											<div class="col-lg-12">
												<span class=""> Velachery, Chennai</span> 
												</div></div>
												<div class="row">
												<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
												</div>
										</div>
									</div>
									
									<div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Besant technologies</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
											<div class="col-lg-12">
												<span class=""> Velachery, Chennai</span> 
												</div></div>
												<div class="row">
											<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
												</div>
										</div>
									</div>
								<div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Kernel Zone Solution</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
											<div class="col-lg-12">
												<span class=""> Velachery, Chennai</span> 
												</div></div>
												<div class="row">
											<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
												</div>
										</div>
								  </div>
								<div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Ayser Info Services</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
											<div class="col-lg-12">
												<span class=""> Velachery, Chennai</span> 
												</div></div>
												<div class="row">
											<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
												</div>
										</div>
								</div>
									<div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Kernel Zone Solution</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
												<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
											<div class="col-lg-12">
												<span class=""> Velachery, Chennai</span> 
												</div></div>
												<div class="row">
											<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
												</div>
										</div>
									</div>
								    <div class="col-lg-2 ">
										<div class="other-admin">
										<div class="row">
										<div class="col-lg-12">
											<strong class="other-admin-name"><a href="" class="">Solprocom Computers</a></strong>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-12">
											<span class="star-rating ">
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
													<i class="fa fa-star scolor"></i>
												</span>
											</div></div>
											<div class="row">
												<div class="col-lg-12">
													<span class=""> Velachery, Chennai</span> 
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12 ">
												<a href=""><span class="pull-right"><span class="glyphicon glyphicon-earphone"></span>contact</span></a>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="mainblock">
								<h3>Find Services Related to Administration Training</h3>
								<div class="service-admin-training">
									<ul class="service-nav">
										<li class=""><a href="">Nearby</li>
										<li class=""><a href="">Locality</li>
										<li class=""><a href="">By City</li>
									</ul>
						         <div class="row ">
									<div class="col-lg-12 hide">
									
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Perungudi</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Velachery</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Velachery</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Velachery</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Velachery</a></span>
										</div>
									
									</div>
									<div class="col-lg-12">
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">T. Nagar</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Velachery</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Perungudi</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Mylapore</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">Vadapalani</a></span>
										</div>
										<div class="col-lg-3">
											<span class="service-admin-font"><a href="">West Mambalam</a></span>
										</div>
									</div>
								</div>
				</div>
			</div>
		</div>