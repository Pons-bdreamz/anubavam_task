<?php

class Model {

    protected $db;

    public function __construct() {
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->db = new PDO($dsn, DB_USER, DB_PASSWORD);
               $this->db->query('SET SQL_BIG_SELECTS=1'); 
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function __destruct() {
        $this->db = null;
    }

    public function generatefooter() {
        /* get the parenet menu details */
        $m_fields = "m.bdz_menu_id,m.bdz_menu_name";
        $m_where = "and m.bdz_menu_type_id = ? and m.bdz_menu_parent_id = ? and m.bdz_menu_active = ? and m.bdz_menu_deleted = ? order by m.bdz_menu_position asc";
        $m_params = array(2, 0, 1, 0);
        $query = "select $m_fields from bdz_menu m where 0=0 $m_where";
        $query_stmt = $this->db->prepare($query);
        $query_stmt->execute($m_params);
        $records = $query_stmt->FetchAll();
        $menu = array();
        $data = array();
        if ($records) {
            for ($i = 0; $i < count($records); $i++) {
                $menu_details = array();
                $menu_details['parentmenu'] = array();
                $menu_details['parentmenu']['menu_name'] = !empty($records[$i]['bdz_menu_name']) ? $records[$i]['bdz_menu_name'] : "";
                $s_fields = "m.bdz_menu_name,u.url_alias_name";
                $s_where = "and m.bdz_menu_type_id = ? and m.bdz_menu_parent_id = ? and m.bdz_menu_active = ? and m.bdz_menu_deleted = ? order by m.bdz_menu_position asc";
                $s_params = array(1, 0, 2, $records[$i]['bdz_menu_id'], 1, 0);
                $query = "select $s_fields from bdz_menu m join bdz_url_alias u on (m.bdz_menu_url_alias_id = u.url_alias_id and u.url_alias_active = ? and u.url_alias_deleted = ?) where 0=0 $s_where";
                $query_stmt = $this->db->prepare($query);
                $query_stmt->execute($s_params);
                $results = $query_stmt->FetchAll();
                $menu_details['childmenu'] = array();
                if ($results) {
                    for ($j = 0; $j < count($results); $j++) {
                        $child_menu = array();
                        $child_menu['menu_name'] = !empty($results[$j]['bdz_menu_name']) ? $results[$j]['bdz_menu_name'] : "";
                        $child_menu['url_alias'] = !empty($results[$j]['url_alias_name']) ? $results[$j]['url_alias_name'] : "";
                        $menu_details['childmenu'][] = $child_menu;
                    }
                }
                $data[] = $menu_details;
            }
        }
        $menu['footermenu'] = $data;
        return $menu;
    }

    /* Generate URL */

    public function getsiteurl($url) {
      $ex_cur_url = explode(".",$url);
        if(count($ex_cur_url)>1)
        {
			if($ex_cur_url[1]=="xml")
			{
				$redirect_url = "management/sitemapgenerate/$url";
				return $redirect_url;
			}
        }
 else {
        $params = array($url);
        $notfound = BASE_URL . "/index";
        // First Check the Alias name 
        $url_org = $this->db->prepare('select url_alias_name,url_content_type_id,url_content_id,url_original_name from bdz_url_alias where url_alias_name = ?  and url_alias_deleted = 0 ');
        $url_org->execute($params);
        $result = $url_org->fetchAll();
        $i = 0;
		
        // if url alias name is not found chk url original name
        if (count($result) == 0) {
            $params1 = array($url);
            $url_org = $this->db->prepare('select url_alias_name,url_original_name from bdz_url_alias where url_original_name = ?  and url_alias_deleted = 0 ');
            $url_org->execute($params1);
            $result = $url_org->fetchAll();

            // if original name found set the alias name permenant link
            if (count($result) > 0) {
                $rurl = BASE_URL . "/" . $result[0]['url_alias_name'];
                //header("HTTP/1.1 301 Moved Permanently", TRUE, 301);
                header("Location:" . $rurl, TRUE, 301);
                exit();
                $redirect_url = $result[0]['url_original_name'];
            } else {
                /* if original name not found chk admin or user module 
                  if admin/user module skip this checking
                  else chk url have any paramas
                 */
                $city_url = "";
                $first_part = explode("/", $url);
                if (!preg_match('/^user$/', $first_part[0]) && !preg_match('/^management$/', $first_part[0]) &&  !preg_match('/^search/', $first_part[0]) && !preg_match('/^business/', $first_part[0]) && !preg_match('/^report$/', $first_part[0]) && !preg_match('/^user$/', $first_part[0]) && ((strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest') &&  !preg_match('/^sitebusiness/', $first_part[0]))){
                    $find_city = true;
                    $cities = true;
                    $get_city = true;
                    $area_url = "";
                    $params = "";
                    $user_data = true;
                    if (isset($_SESSION['user_ip_city_link']) && !empty($_SESSION['user_ip_city_link'])) {
                       if (in_array($_SESSION['user_ip_city_link'], $first_part)) {
                           $cities = false;
                            if (isset($_SESSION['user_location']['user_city_no']) && !empty($_SESSION['user_location']['user_city_no'])) {
                                                                $find_city = false;
                                                                array_splice($first_part,0,1);
								$city_url = "/city-" . $_SESSION['user_location']['user_city_no'];
								$redirect_url = "index";
                            }
                        }
                       
                       
                    }
                    
                    if ($find_city) {
			$url_city = $this->db->prepare('select url_alias_name,url_page_title,url_content_id,url_original_name from bdz_url_alias where url_alias_name = ?  and url_content_type_id=8 and url_alias_deleted = 0 ');
                        $url_city->execute(array($first_part[0]));
                        $city_result = $url_city->fetchAll();
                        if (count($city_result) > 0) {
                            if ($cities) {
                                unset($_SESSION['user_ip_city']);
                                unset($_SESSION['user_ip_city_link']);
                                unset($_SESSION['user_location']);
                                $_SESSION['user_location']['user_city_no'] = $city_result[0]['url_content_id'];
                                $_SESSION['user_location']['country_timezone'] = "";
                                $_SESSION['user_location']['user_city'] = "";
                                $_SESSION['user_ip_city'] = $city_result[0]['url_page_title'];
                                $_SESSION['user_ip_city_link'] = $city_result[0]['url_alias_name'];
                            } else {
                                unset($_SESSION['user_location']['user_city_no']);
                                $_SESSION['user_location']['user_city_no'] = $city_result[0]['url_content_id'];
                            }
                            $city_url = "/city-" . $_SESSION['user_location']['user_city_no'];
                            $key = array_search($_SESSION['user_ip_city_link'], $first_part);
                            array_splice($first_part, $key, 1);
                            $redirect_url = "index";
                        } else {
                            $url_user = $this->db->prepare('select url_alias_name,url_page_title,url_content_id,url_original_name from bdz_url_alias where url_alias_name = ?  and url_content_type_id=12 and url_alias_deleted = 0 ');
                            $url_user->execute(array($first_part[0]));
                             $user_result = $url_user->fetchAll();
                            if (count($user_result) > 0) {
                                $url = $user_result[0]['url_original_name'];
                                $key = array_search($first_part[0], $first_part);
                                array_splice($first_part, $key, 1);
                                $other_url = implode("/", $first_part);
                                $redirect_url = $url."/".$other_url;
                                $user_data = false;
                               
                            }
                            else
                                 $redirect_url = "/notfound1";
                        }
                    }
                    if(empty($city_url) && $user_data)
                    {
                        if(isset($_SESSION['user_ip_city_link']) && !empty($_SESSION['user_ip_city_link']))
                         {
                             $curl = $_SESSION['user_ip_city_link'];
                         }
                        else {
                            $curl = getbasecity();
                        }
                          
                          if(!empty($curl)) {
                                 $burl= BASE_URL."/".$curl."/".$url;  
                                 header("location:".$burl);
                            }
                    }
                    if (count($first_part) > 0 && $user_data) {
                         /* if already existing country not found not in (8,9) */
                        $url_city = $this->db->prepare('select url_alias_name,url_content_id,url_original_name from bdz_url_alias where url_alias_name = ? and url_content_type_id not in (8)  and url_alias_deleted = 0 ');
                        $url_city->execute(array($first_part[0]));
                        $city_result = $url_city->fetchAll();
                        if (count($city_result) > 0) {
                            array_splice($first_part, 0, 1);
                            $params = implode("/", $first_part);
                            $redirect_url = $city_result[0]['url_original_name'];
                        } else {
                            $spl_str = strripos($first_part[0], '-in-');
                            $con_area = "";
                            if ($spl_str) {
                                $con_area = substr($first_part[0], $spl_str + 4);
                                $org_url_cont = substr($first_part[0], 0, $spl_str);
                            } else {
                                $org_url_cont = $first_part[0];
                            }
                            $url_res = $this->db->prepare('select url_alias_name,url_original_name from bdz_url_alias where url_alias_name = ? and url_content_type_id not in (8,9)  and url_alias_deleted = 0 ');
					        $url_res->execute(array($org_url_cont));
                            $chk_result = $url_res->fetchAll();
				if (count($chk_result) > 0) {
                                $url_res = $this->db->prepare('select url_content_type_id,url_content_id,url_alias_name,url_original_name from bdz_url_alias where url_alias_name = ? and url_content_type_id = 9  and url_alias_deleted = 0 ');
                                $url_res->execute(array($con_area));
                                $chk_result_area = $url_res->fetchAll();
                                if (count($chk_result_area) > 0) {
                                    /* if area is available pass the area params to the controller */
                                    array_splice($first_part, 0, 1);
                                    //$area_url = "/ar" . $chk_result_area[0]['url_content_type_id'] . "-" . $chk_result_area[0]['url_content_id'];
									$area_url = "/ar" ."-". $chk_result_area[0]['url_content_id'];
                                    $url = $chk_result[0]['url_original_name'];
                                    $redirect_url = $url;
                                } else {
									array_splice($first_part, 0, 1);
                                    $url = $chk_result[0]['url_original_name'];
                                    $redirect_url = $url;
                                }
                            } else {
                                $redirect_url = "/notfound";
                            }
                        }

                        if ($redirect_url != "/notfound") {
                            if (count($first_part) > 0)
                                $params = "/" . implode("/", $first_part);

                            $redirect_url = $redirect_url . $city_url . $area_url . $params;
                        }
                    }
                }
               else {
                    //Invalid URL
                   $redirect_url = $url;
                } 
            }
        } else {
            //Correct Alias Name
         
		
           if($result[0]['url_content_type_id']==8 || $result[0]['url_content_type_id']==0 || $result[0]['url_content_type_id']==12)
           {
                $redirect_url = $result[0]['url_original_name'];
                
               if($result[0]['url_content_type_id']==8) {
                  if(isset($_SESSION['user_location']['user_city_no']))
                  unset($_SESSION['user_location']['user_city_no']);
                if(isset($_SESSION['user_ip_city_link']))
                    unset($_SESSION['user_ip_city_link']);
                
                $_SESSION['user_location']['user_city_no'] = $result[0]['url_content_id'];
                $_SESSION['user_ip_city_link'] = $result[0]['url_alias_name'];
               }
               
           }
            else {
                
             
              if($result[0]['url_content_type_id'] == 11)
              {
                  $curl = $this->businesscityassign($result[0]['url_content_id']);
				  if(!empty($curl))
                  {
                     
                       $burl= BASE_URL."/".$curl."/".$url;  
                       header("location:".$burl);
                  }
                  else
				  {
					  $url = !empty($result[0]['url_original_name'])?$result[0]['url_original_name']:"/index";
                      $redirect_url = $url;
				  }
              }
              else {
               /* add the city url to only non static pages */   
               if($result[0]['url_content_type_id'] != 10){    
                   if(isset($_SESSION['user_ip_city_link']) && !empty($_SESSION['user_ip_city_link']))
                       $curl = $_SESSION['user_ip_city_link'];
                   else
                      $curl = getbasecity();
                   
                   $burl= BASE_URL."/".$curl."/".$url;  
                   header("location:".$burl);
			   }
			   else
			   {
					  $url = !empty($result[0]['url_original_name'])?$result[0]['url_original_name']:"/index";
                      $redirect_url = $url;

			   }
              }
               
             
            }
           
         
        }
        
      return $redirect_url;
 }

    }
    
    /* business id city getting method */
    public function businesscityassign($bus_id)
    {
                  $url_org = $this->db->prepare('select business_area,business_city from bdz_business where business_id = '.$bus_id);
                  $url_org->execute();  
                  $bresult = $url_org->fetchAll();
				  if(count($bresult)>0)
                  {
                      $url_org = $this->db->prepare('select url_alias_name,url_content_id from bdz_url_alias where url_content_type_id = 8 and url_content_id = '.$bresult[0]['business_city']);
                      $url_org->execute();  
                      $uresult = $url_org->fetchAll();
                      if(count($uresult)>0)
                      {
                          $curl = $uresult[0]['url_alias_name'];
                          unset($_SESSION['user_ip_city_link']);
                          unset($_SESSION['user_location']['user_city_no']);
                          $_SESSION['user_ip_city_link'] = $curl;
                          $_SESSION['user_location']['user_city_no'] =$uresult[0]['url_content_id'];
                      }
                      else 
                          $curl = "";
                  }
                  else
                      $curl = "";
                  
                  return $curl;
    }

    /* get the ip related functionalities */

    public function getipdetails($table, $cwhr, $params) {
        $sql = "SELECT * FROM $table WHERE $cwhr";
        $pr_stmt = $this->db->prepare($sql);
        $pr_stmt->execute($params);
        $countrylist = $pr_stmt->fetchAll();
        $country = array();
        if (count($countrylist) > 0) {
            $country['iso'] = $countrylist[0]['country_code'];
            $country['city'] = $countrylist[0]['city_name'];
            $country['timezone'] = $countrylist[0]['time_zone'];
            $country['zip_code'] = $countrylist[0]['zip_code'];
            $country['region_name'] = $countrylist[0]['region_name'];
        }
        return $country;
    }

    public function getipcountry($cwhr, $params) {
        $sql = "SELECT cu.url_alias_name,c.city_name,c.city_id,s.state_name,s.state_id,ct.country_id,ct.country_name FROM  bdz_cities c left join bdz_district d on c.district_id = d.district_id left join bdz_states s on s.state_id = d.state_id left join bdz_country ct on s.country_id = ct.country_id join bdz_url_alias cu on (cu.url_content_type_id = 8 and cu.url_content_id = c.city_id)"
                . " where $cwhr";
        $pr_stmt = $this->db->prepare($sql);
        $pr_stmt->execute($params);
        $countrylist = $pr_stmt->fetchAll();
       // print_r($countrylist);
        if (count($countrylist) > 0) {
            $country = array();
            $country['city_name'] = $countrylist[0]['city_name'];
            $country['city_id'] = $countrylist[0]['city_id'];
            $country['city_url'] = $countrylist[0]['url_alias_name'];
            $country['state'] = $countrylist[0]['state_name'];
            $country['country'] = $countrylist[0]['country_name'];
        } else {
            $country = array();
        }
        return $country;
    }
    
    
   

}

?>