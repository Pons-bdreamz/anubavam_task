<?php
	
	/*
		Doc Created by : Pons
		Doc Created On : 
		DOC Reviewed By : 
	*/
	
	/* Required default files */
	require_once(ROOT.DS.'config'.DS.'config.php');
	require_once(ROOT.DS.'core'.DS.'functions.php');
	require_once(ROOT.DS.'helpers'.DS.'url_helper.php');
	require_once(ROOT.DS.'helpers'.DS.'form_validation.php');
	require_once(ROOT.DS.'vendor'.DS.'autoload.php');
	
	//Autoloaded Classeso
	
	//function __autoload($className)
        function my_autoloader($className)
	{
		if(file_exists(ROOT.DS.'core'.DS.strtolower($className).'.php'))
		{
			require_once(ROOT.DS.'core'.DS.strtolower($className).'.php');
		}
		else if(file_exists(ROOT.DS.'application'.DS.'controllers'.DS.strtolower($className).'.php')){
			require_once(ROOT.DS.'application'.DS.'controllers'.DS.strtolower($className).'.php');
		}
		else if(file_exists(ROOT.DS.'application'.DS.'models'.DS.strtolower($className).'.php')){
			require_once(ROOT.DS.'application'.DS.'models'.DS.strtolower($className).'.php');
		}
		else if(file_exists(ROOT.DS.'core'.DS.'sql.php')){
			require_once(ROOT.DS.'core'.DS.'sql.php');
		}
		
	}
        
        spl_autoload_register('my_autoloader');


	
	Router::route($url);
	
?>