<?php
	class Controller extends Application{
		 protected $controller;
		 protected $action;
		 protected $models;
		 protected $view;
         protected $base_url;
         public function __construct($controller,$action){
              parent::_construct();
			  $this->controller=$controller;
			  $this->action=$action;
              $this->view=new View(); 
			          
		 
		 }
		 
                 
                public function solr_configuration(){
                    $solrconfig = array(
                   'endpoint' => array(
                   'localhost' => array(
                   'host' => SITE_HOST,
                   'port' => 8983,
                   'path' => '/solr',
                   'core' => 'portal'
                   ))); 
                    return $solrconfig;
                }
                 
                 
		 //Load modal class 
		 protected function load_model($model){
			 
			 if(class_exists($model))
			 {
				$this->models[$model] = new $model();
				return $this->models[$model];
			 }
			
		 }
		 //Get the instance of the loaded model class
		 protected function get_model($model)
		 {
			 if(is_object($this->models[$model])){
					return $this->models[$model];
			 }
			 else
			 {
				 return false;
			 }
		 }
		 
		 //Return View Object
		 protected  function get_view(){
			  return $this->view;
		 }
		 
		  // Pagination Calculation
		 protected function pagination($rows,$page)
		 {
					   $total = $page * $rows;
					   $start = $total - $rows;
					   $limit_stmt =" limit $start,$rows";
					   return $limit_stmt;
		 }
		 
		
		 
		 public function getcountrydetails($ip_address)
		 {
                    // $ip_address = '183.82.173.128';
                     $modal = new Model();
                     getbasiccity($ip_address, $modal);
		     
		 }
                 
                  public function footergeneration()
		 {
			 $modal = new Model();
			 $this->footer_details = $modal->generatefooter();
		 }
                 
                 
    /* 
	* Solr - Insert the new document into solr server 
	* Calling procedure :  this method call from any controller 
	* $params = array(1,"test","test","test","test");
    * $this->createsolrreq($params);
	*/
    public function createsolrreq($params)
    {
        $headers = array('Content-Type:application/xml');  
        $client = new Solarium\Client($this->solrconfig);  
		$client->getEndpoint()->setAuthentication("solradmin","143@solr");
		
        $update = $client->createUpdate();      
        $catdoc = $update->createDocument();
        $catdoc->id =$params[0];
        $catdoc->name = $params[1];
        $catdoc->urlalias = $params[2];
        $catdoc->pagetitle = $params[3];
        $update->addDocument($catdoc);
        $update->addCommit();
        $request = $client->createRequest($update);
        $request->addHeaders($headers);
        $result = $client->executeRequest($request);
        return true;        
    }
    
  
	
	}
?>