<?php
class  Router {
	
	 public static function route($url)
	 {
		    $url_array = array();
			$url_array = explode("/",$url);
                        
                        
			// The first part of the URL is the controller name
			$controller = isset($url_array[0]) ? trim($url_array[0]):'';
			array_shift($url_array);
			//The second part is the method name
			$action = isset($url_array[0]) ?trim($url_array[0]):'';
			array_shift($url_array);
			//The third part is the arguments
			$query_string = $url_array;
			//if the controller is empty, redirect to default controller
			if(empty($controller))
			{
				$controller = DEFAULT_CONTROLLER;
			}
			if(empty($action)){
				$action = "index";
			}
			$controller_name = $controller;
			$controller = ucwords($controller);
            if(class_exists($controller))
			{
                    if((int)method_exists($controller,$action)){
                        $dispatch = new $controller($controller,$action);
                        call_user_func_array(array($dispatch,$action),$query_string);
					}
					else {
							// If the controller and action is not correct redirect to default page
							$dispatch = new $controller($controller,"index");
							$action = "index";
							call_user_func_array(array($dispatch,$action),$query_string);
							/* get the error message*/
						}
			}
			else
			{
                $controller = "index";
				$dispatch = new $controller($controller,"index");
				$action = "index"; 
                call_user_func_array(array($dispatch,$action),$query_string);
			}
			
	 }
	
	 
	
	 
}

?>