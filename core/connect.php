<?php
class Database {
	
	private $connection;
	public $arg_query;
	public $db;
	
	
	function __construct() {
		$this->open_connection();
		date_default_timezone_set('Asia/Kolkata');
  }

	public function open_connection() {
		$this->db = new PDO('mysql:host=localhost;dbname=besantte_stagbems;', 'besantte_stagbem', '143@StagBEMS');
		$this->db->query('SET SQL_BIG_SELECTS=1'); 
	
	}
	
	public function close_connection(){
		if(isset($this->db)) {
			unset($this->db);
		}
	}

	//get total no of rows 
	public function total_rows($table,$where=null) {
			if($where == null)
					$where = " 0=0 ";
			$stmt = $this->db->prepare("SELECT * FROM $table  where $where");
			$stmt->execute();
			$row_count = $stmt->rowCount();
			return $row_count;
	}
	//chk if the row is exists or not
	public function chk_records($table,$where=null) {
		    
			if($where == null)
					$where = " 0=0 ";
			if($table == null)
					$table = "bems_user_enquiry";
				
			$stmt = $this->db->prepare("SELECT * FROM $table  where $where");
			$stmt->execute();
			$row_count = $stmt->rowCount();
			return $row_count;
	}
	// Get all the records
	public function all_records($table,$where=null,$fields=null)
	{
		 	if($where==null)
				$where =  "0=0";
			if($fields == null)
					$fields = "*";
			
		    $stmt = $this->db->prepare("SELECT $fields FROM $table where $where");
			$stmt->execute();
			$row_count = $stmt->fetchAll();
			return $row_count;
	}
	
	// Get all the Enquiry records
	public function enq_records($where=null)
	{
		 	if($where==null)
				$where =  "0=0";
			$stmt = $this->db->prepare("SELECT * FROM bems_user_enquiry e left join bems_followup f on f.followup_enquiry_id = e.enquiry_id where $where");
			$stmt->execute();
			$row_count = $stmt->fetchAll();
			return $row_count;
	}
	
	// function for insert invoice

    public function insert_invoice($tables,$keys,$values,$value_keys)

    {
        $stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
        $res=$stmt->execute($values);
        if($res)
        {
            $last_id=$this->db->lastInsertId();
            return $last_id;
        }    
        else
        {
            return false;
        }
     

    }
	
	// Common function for insert query
	public function insert_rows($tables,$keys,$values,$value_keys)
	{
		$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
		$res=$stmt->execute($values);
		if($res)
		{
			$last_id=$this->db->lastInsertId();
			return true;
		}	
		else{
			return false;
		} 
		
	}
        
        // Common function for return insert insert query id
	public function insert_filerows($tables,$keys,$values,$value_keys)
	{
          	$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
                $res=$stmt->execute($values);
		if($res)
		{
			$last_id=$this->db->lastInsertId();
			return $last_id;
		}	
		else{
			return false;
		} 
		
	}
	
	
	/* 
		Enquiry Form Insertion
		     ---- insert the records into enquiry table
			 ---- insert the records into followup table		
	*/
	public function enquiry_insert($tables,$keys,$values,$value_keys,$follow_values)
	{
		try {  
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
				$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
				$res=$stmt->execute($values);
				$last_id=$this->db->lastInsertId();
				array_unshift ($follow_values,$last_id);
				$stmt = $this->db->prepare("INSERT INTO bems_followup (followup_enquiry_id,followup_date,followup_status,followup_comments,followup_created_user,followup_created_date)  VALUES (?,?,?,?,?,?)");
				$res=$stmt->execute($follow_values);
				$last_fol_id=$this->db->lastInsertId();
				$this->db->commit();
			    return $last_id;
		}
		catch (Exception $e) 
		{
			  $this->db->rollBack();
			  return false;
		}
		
	}
		/* 
		Multiple Table Insertion
		     ---- insert the records into First table
			 ---- insert the records into second table	based on first table id
	*/
	public function multiple_insert($first_table_name,$field_name,$value_keys,$values,
	$sec_table_name,$sec_field_name,$sec_values_keys,$second_params,$skill_table_name,$skill_field_name,$skill_values_keys,$skill_field_values)
	{
		try {  
		//print_r($second_params);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
				$stmt = $this->db->prepare("INSERT INTO $first_table_name ($field_name)  VALUES ($value_keys)");
				$res=$stmt->execute($values);//user table
				$last_id=$this->db->lastInsertId();
				$second_params[]=$last_id;
				$stmt = $this->db->prepare("INSERT INTO $sec_table_name ($sec_field_name)  VALUES ($sec_values_keys)");
				$res=$stmt->execute($second_params);
				//print_r($skill_field_values);
				for($i=0;$i<count($skill_field_values);$i++)
				{
				
				$stmt = $this->db->prepare("INSERT INTO $skill_table_name ($skill_field_name)  VALUES ($skill_values_keys)");
				$res=$stmt->execute(array($skill_field_values[$i],1,$last_id));
				
				}
				$this->db->commit();
			    return true;
				
		}
		catch (Exception $e) 
		{
                    echo $e->getMessage();
                    die;
			 $this->db->rollBack();
			  return false;
		}
		
	}
	//update
	public function update_keyskills($key_skills,$user_id,$extra = "",$skill_type = 1)
	
		{try {  
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
			$stmt = $this->db->prepare("update bems_user_skills set skill_active = 0 where  user_id = $user_id and skill_id not in ($key_skills) $extra");
			$res = $stmt->execute();
			$where = "user_id = $user_id";
			$field_values = "skill_id,skill_active";
			$table_name = "bems_user_skills";
			$result = $this->all_records($table_name,$where,$field_values);
			$data=array();
			for($i=0;$i<count($result);$i++)
			{
				$data[$result[$i]['skill_id']]= $result[$i]['skill_active'];
			}
			$res=array();
			$key_skills=explode(',',$key_skills);
			$skill_flied = 'skill_id,user_id,skill_type';
			$skill_value='?,?,?';
			for($i=0;$i<count($key_skills);$i++)
			{
				if(!array_key_exists($key_skills[$i],$data)){
				$stmt = $this->db->prepare("INSERT INTO bems_user_skills ($skill_flied)  VALUES ($skill_value)");
				$res = $stmt->execute(array($key_skills[$i],$user_id,$skill_type));
				}
				else{
					if($data[$key_skills[$i]] == 0 ){
						$s_id = $key_skills[$i];
						$stmt = $this->db->prepare("update bems_user_skills set skill_active = 1 
						where user_id = $user_id and skill_id = $s_id");
						$res = $stmt->execute();
					}
				}
				
			
			}$this->db->commit();
			    return true;
				
		}
		catch (Exception $e) 
		{
			 $this->db->rollBack();
			  return false;
		}		
		
		}
		
		


	/* New JD Creation
		 -- insert the key skills values into jd skills table
	*/
	public function jd_insert($tables,$keys,$values,$value_keys,$keyskills)
	{
		try {  
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
				$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
				$res=$stmt->execute($values);
				$last_id=$this->db->lastInsertId();
				for($j=0;$j<count($keyskills);$j++){
					$stmt = $this->db->prepare("INSERT INTO bems_jd_key_skills (jd_id,key_skill_id)  VALUES (?,?)");
					$res=$stmt->execute(array($last_id,$keyskills[$j]));
				}
				$this->db->commit();
			    return true;
		}
		catch (Exception $e) 
		{
			  $this->db->rollBack();
			  return false;
		}
		
	}
		/*Followup Form Insertion
		     ---- insert the records into Followup table
			 ---- update the followup date and status in enquiry table
	*/
	public function followup_insert($tables,$keys,$values,$value_keys,$follow_values)
	{
		try {  
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
				$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
				$res=$stmt->execute($values);
				$ustmt = $this->db->prepare("update bems_user_enquiry set enquiry_followup_date = ?,enquiry_followup_status = ?,enquiry_followup_created_on = ?,enquiry_followup_comments = ?  where enquiry_id = ?");
				$ustmt->execute($follow_values);
				$this->db->commit();
			   return true;
		}
		catch (Exception $e) 
		{
			echo $e->getMessage();
			 $this->db->rollBack();
			  return false;
		}
		
	}
	/*
			Enquiry Form Updating time
					---- Update the enquiry form
					---- chk the prev date and status if the date is greater than current followupdate
					---- update the followupdate and status column
					---- else update other columns
	*/
	public function enquiry_edit_form($table,$field_values,$where_col_name,$user_id)
	 {
		 try {  
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->beginTransaction();
			// Chk the date
			$chk_where = "enquiry_id = ".$field_values['where'];
			$res_array=$this->all_records($table,$chk_where);
			$prev_followup_date = strtotime($res_array[0]['enquiry_followup_date']);
			$curr_followup_date = strtotime($field_values['enquiry_followup_date']);
			$calc = $curr_followup_date - $prev_followup_date;
			if($calc <= 0)
			{
				unset($field_values['enquiry_followup_date']);
				unset($field_values['enquiry_followup_created_on']);
				unset($field_values['enquiry_followup_status']);
			}
			$res=array();
			$fields="";
		  	foreach($field_values as $key => $val)
			{
				if($key != 'where')
				{
					$fields.=" $key = ?,";
				}
				
				$res[]=$val;
			}
			
			$fields=substr($fields, 0, -1);
			$stmt = $this->db->prepare("update $table set $fields where $where_col_name");
			$stmt->execute($res);
			if($calc > 0)
			{
				$upstmt = $this->db->prepare("INSERT INTO bems_followup (followup_enquiry_id,followup_date,followup_status,followup_comments,followup_created_user,followup_created_date)  VALUES (?,?,?,?,?,?)");
				$res=$upstmt->execute(array($field_values['where'],$field_values['enquiry_followup_date'],$field_values['enquiry_followup_status'],$field_values['enquiry_comments'],$user_id,date('Y-m-d H:i:s')));
			}
			$this->db->commit();
		   return true;
		}
		catch (Exception $e) 
		{
		
			 $this->db->rollBack();
			  return false;
		}
	}
	/* 
		 Enquiry Form Assign Time
		     ---- Update the enquiry table assigned employee column
			 ---- chk the followup date column if the value is null then 
			 ---- insert the new followup record into followup table
			 ---- Update the followup status and date column in enquiry table
	*/
	public function enquiry_update($table,$field_values,$where_col_name,$enquiry_id,$user_id,$fcomments = "")
	 {
		 try {  
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->beginTransaction();
			$res=array();
			$fields="";
		  	foreach($field_values as $key => $val)
			{
				if($key != 'where')
				{
					$fields.=" $key = ?,";
				}
				
				$res[]=$val;
			}
			$fields=substr($fields, 0, -1);
			$stmt = $this->db->prepare("update $table set $fields where $where_col_name");
			$stmt->execute($res);
			$where = 'enquiry_followup_date IS NULL and enquiry_id IN ('.implode(",",$enquiry_id).')';
			$res_array=$this->all_records($table,$where);
			$total_count = count($res_array);
			for($k=0;$k<count($res_array);$k++)
			{
				//$followup_date = date('Y-m-d', strtotime(' +1 day'));
				$followup_date = date('Y-m-d');
				$ustmt = $this->db->prepare("update $table set enquiry_followup_date = '".$followup_date."'  where enquiry_id = ?");
				$ustmt->execute(array($res_array[$k]['enquiry_id']));
				$upstmt = $this->db->prepare("INSERT INTO bems_followup (followup_enquiry_id,followup_date,followup_created_user,followup_created_date,followup_comments)  VALUES (?,?,?,?,?)");
				$res=$upstmt->execute(array($res_array[$k]['enquiry_id'],$followup_date,$user_id,date('Y-m-d H:i:s'),$fcomments));
			}
			if($total_count == 0)
			{
				if(!empty($fcomments))
				{
					for($u=0;$u<count($enquiry_id);$u++)
					{
					$followup_date = date('Y-m-d');
					$upstmt = $this->db->prepare("INSERT INTO bems_followup (followup_enquiry_id,followup_date,followup_created_user,followup_created_date,followup_comments)  VALUES (?,?,?,?,?)");
					$res=$upstmt->execute(array($enquiry_id[$u],$followup_date,$user_id,date('Y-m-d H:i:s'),$fcomments));
					}
				}
			}
				
				$this->db->commit();
			   return true;
		}
		catch (Exception $e) 
		{
			 $this->db->rollBack();
			  return false;
		}
	}
	
	// Spam updation query
	public function spam_update_record($field_values,$where_col_name,$param)
	 {
		    $res=array();
			$fields="";
			foreach($field_values as $key => $val)
			{
				if($key != 'where')
				{
					$fields.=" $key = ?,";
				}
				
				$res[]=$val;
			}
			$fields=substr($fields, 0, -1);
			$stmt = $this->db->prepare("update bems_user_enquiry set $fields where $where_col_name");
			$stmt->execute($res);
			
			
			$followup_date = date('Y-m-d H:i:s');
			//insert one record into spam records table
			$fvalues = array($param[0],date('Y-m-d'),$param[1],date('Y-m-d H:i:s'),$param[2],11);
			
			$upstmt = $this->db->prepare("INSERT INTO bems_followup (followup_enquiry_id,followup_date,followup_created_user,followup_created_date,followup_comments,followup_status)  VALUES (?,?,?,?,?,?)");
			$res=$upstmt->execute($fvalues);
			return true;
	}
	
	
	
	// Common updation query
	public function update_record($table,$field_values,$where_col_name,$sec_where = "")
	 {
		    $update="update $table set";
			$res=array();
			$fields="";
			foreach($field_values as $key => $val)
			{
				if($key != 'where')
				{
					$fields.=" $key = ?,";
				}
				
				$res[]=$val;
			}
                        
                        
                        
			$fields=substr($fields, 0, -1);
			$stmt = $this->db->prepare("update $table set $fields where $where_col_name $sec_where");
                        $stmt->execute($res);
			return true;
	}
	
// fetching the records to drop down box
function dropdown_data($ddata,$dwhere="")
{
	switch($ddata)
	{
		case "course":
				$table="bems_course";
				$fields='course_id,course_name';
				$where = 'course_deleted = 0 and course_active = 1';
				break;
		case 'status':
				$table="bems_status";
				$fields='status_id,status_name';
				$where = 'status_deleted = 0 and status_active = 1 order by status_id desc';
				break;
		case 'user':
				$table="bems_users";
				$fields='user_id,bems_full_name';
				$where = $dwhere.'user_deleted = 0 and bems_user_active = 1 and bems_user_types in (1,2,5) order by user_id desc';
				break;
		case  'source':
				$table="bems_enquiry_source";
				$fields='source_id,source_name';
				$where = 'source_deleted = 0 and source_active = 1';
				break;
		case 'branch':
				$table="bems_branch";
				$fields='branch_id,branch_name';
				$where = $dwhere.'branch_deleted = 0 and branch_active = 1';
				break;
		case 'training':
				$table="bems_training_type";
				$fields='training_type_id,training_type_name';
				$where = 'training_deleted = 0 and training_active = 1';
				break;
		case 'company':
				$table="bems_company";
				$fields='company_id,company_name';
				$where = 'company_deleted = 0 and company_active = 1';
				break;
	}
		$res_array=$this->all_records($table,$where,$fields);
	  return $res_array;
}
  
  // duplicate name checking 
   function chk_duplicate($tablename,$where)
   {
			$stmt = $this->db->prepare("SELECT * FROM $tablename where $where");
			$stmt->execute();
			$row_count = $stmt->rowCount();
			if($row_count > 0)
				return false;
			else
					return true;
   }
  
   // Fetch the enquiry table records 
   function enquiry_records($where =  null,$fields = null)
   {
	 
	       $total_fields = "e.enquiry_id,e.enquiry_name,e.enquiry_emailid,e.enquiry_contact,s.source_name,st.status_name,c.course_name,e.enquiry_comments,b.branch_name,tt.training_type_name,case  when e.enquiry_assigned_employee = 0 then 'Unassigned' else u.bems_full_name end as bems_full_name ,e.enquiry_created_on,e.enquiry_followup_date,e.enquiry_followup_comments as followup_comments,e.enquiry_followup_created_on as followup_created_date";
		   if($fields==null)
			   $total_fields.=$fields;
		   $stmt = $this->db->prepare("SELECT $total_fields FROM  bems_user_enquiry e left join bems_branch b on e.enquiry_branch = b.branch_id left join bems_enquiry_source s on e.enquiry_source = s.source_id left join bems_status st on e.enquiry_followup_status = st.status_id left join bems_course c on e.enquiry_course = c.course_id left join bems_training_type tt on e.enquiry_training_type = tt.training_type_id left join bems_users u on e.enquiry_assigned_employee =  u.user_id  where $where");
			$stmt->execute();
			$res = $stmt->fetchAll();
			return $res;
   }
   
   // Fetch the Followup details record
   function prev_followup_records($where =  null,$fields = null)
   {
	      $total_fields = "u.bems_full_name,s.status_name,f.followup_date,f.followup_comments,f.followup_created_date";
		   if($fields==null)
			   $total_fields.=$fields;
		   $stmt = $this->db->prepare("SELECT $total_fields FROM bems_followup f left join bems_status s on f.followup_status = s.status_id left join bems_users u on f.followup_created_user = u.user_id where $where");
			$stmt->execute();
			$res = $stmt->fetchAll();
			return $res;
   }
  
	// Unused function
    function followup_records($where =  null,$fields = null)
  {
		  $total_fields ="e.enquiry_id,e.enquiry_name,e.enquiry_emailid,e.enquiry_contact,s.source_name,st.status_name,c.course_name,e.enquiry_comments,b.branch_name,tt.training_type_name,case  when e.enquiry_assigned_employee = 0 then 'undefined' else u.bems_full_name end as bems_full_name ,e.enquiry_created_on";
		   if($fields==null)
			   $total_fields.=$fields;
		   $stmt = $this->db->prepare("SELECT $total_fields FROM  bems_user_enquiry e left join bems_branch b on e.enquiry_branch = b.branch_id left join bems_enquiry_source s on e.enquiry_source = s.source_id left join bems_status st on e.enquiry_followup_status = st.status_id left join bems_course c on e.enquiry_course = c.course_id left join bems_training_type tt on e.enquiry_training_type = tt.training_type_id left join bems_users u on e.enquiry_assigned_employee =  u.user_id where $where");
			$stmt->execute();
			$res = $stmt->fetchAll();
			return $res;
  }
  
  // Php Validation Checking
public function chkname($chkvar)
	 {
		 
		if(!preg_match('/^[A-Za-z\s.]{3,200}$/',$chkvar))
		{
			return false;
		}
		return true;
	 }
		
	 // Php API Validation Checking
	public function chkapiname($chkvar)
	 {
		
		if(!preg_match('/^[A-Za-z0-9\s.]{3,200}$/',$chkvar))
		{
			return false;
		}
		return true;
	 }
	 
	 public function chkapinum($chkvar)
	 {
		
		if(!preg_match('/^[0-9]+$/',$chkvar))
		{
			return false;
		}
		return true;
	 }
	 
	 public function chkapimobile($chkvar)
	 {
		
		if(!preg_match('/^\d{10}$/',$chkvar))
		{
			return false;
		}
		return true;
	 }
	 
	 public function chkapileadid($chkleadid,$table)
	 {
		$stmt = $this->db->prepare("SELECT api_lead_id FROM $table where api_lead_id = $chkleadid");
		$stmt->execute();
		$res = $stmt->rowCount();		
		if($res==0)
		{
			return false;
		}
		return true;
		 
	 }

	 // Password Validation
	  public function chkpass($orgpwd,$repwd)
	 {
			if(!preg_match('/^[A-Za-z0-9!@#$%^&*\(\)\/\\-_+|\{\}\":;\',.\<\>]{6,20}$/',$orgpwd))
			{
					return false;
			}
		else{
			 if(trim($orgpwd)!=trim($repwd))
			 {
				 return false;
			 }
		}
		return true;
	 }
	 
	 // Email validation
	   public function chkemail($umail)
	 {
		 if(!preg_match('/^[_a-z0-9A-Z-]+(\.[_a-z0-9A-Z-]+)*@([a-z0-9A-Z-]{3,100})+(\.[a-z0-9A-Z-]+)*(\.[a-zA-Z-]{2,4})$/',$umail))
		{
			return false;
		}
		
		return true;
	 }

 
	 //Get the Followup Log Count 
	 public function getfollowup_count($where=null,$ewhr = null)
	{
		 	$sqlstmt = "(SELECT count(e.enquiry_id) as lcount FROM bems_followup f left join bems_user_enquiry e on f.followup_enquiry_id = e.enquiry_id left join bems_users u on f.followup_created_user = u.user_id where $where ) union all(select count(e.enquiry_id)  as lcount from bems_user_enquiry e where e.enquiry_assigned_employee = 0 and e.enquiry_deleted = 0 $ewhr)";
			$stmt = $this->db->prepare($sqlstmt);
			$stmt->execute();
			$row_count = $stmt->fetchAll();
			return $row_count;
	}

         
         // Get total Count from select querry
        public function get_count($select,$where=array()) {
			$stmt = $this->db->prepare($select);
			$stmt->execute($where);
			$row_count = $stmt->rowCount();
			return $row_count;
	}
	 
	 // Get all the Followup records
	public function getfollowup_logs($where=null,$ewhr = null ,$limit = null)
	{
		 	if($where==null)
				$where =  "0=0";
			
			$sqlstmt = "(SELECT e.enquiry_id,e.enquiry_contact,e.enquiry_name,f.followup_created_date,f.followup_status,s.status_name,u.bems_full_name,eu.bems_full_name as assigned_emp,f.followup_comments,'2' as 'source' FROM bems_followup f left join bems_user_enquiry e on f.followup_enquiry_id = e.enquiry_id left join bems_users eu on e.enquiry_assigned_employee = eu.user_id left join bems_status s on f.followup_status = s.status_id left join bems_users u on f.followup_created_user = u.user_id where $where ) union all(select e.enquiry_id,e.enquiry_contact,e.enquiry_name,e.enquiry_created_on,c.course_name,e.enquiry_deleted,e.enquiry_created_user,e.enquiry_comments,e.enquiry_source,'1' as 'source' from bems_user_enquiry e left join bems_course c on e.enquiry_course = c.course_id where e.enquiry_assigned_employee = 0 and e.enquiry_deleted = 0 $ewhr ) order by followup_created_date desc $limit";
			// echo $sqlstmt;
			$stmt = $this->db->prepare($sqlstmt);
			$stmt->execute();
			$row_count = $stmt->fetchAll();
			return $row_count;
	}
	 
	 
	 public function getopenenquiry($where="")
	 {
			/*	$stmt = $this->db->prepare(" SELECT * FROM bems_user_enquiry where enquiry_id not in(select followup_enquiry_id from bems_followup group by followup_enquiry_id) and enquiry_deleted = 0 order by enquiry_id desc");
		*/
			$stmt = $this->db->prepare(" SELECT e.enquiry_id,e.enquiry_name,e.enquiry_contact,c.course_name,e.enquiry_created_on FROM bems_user_enquiry e left join bems_course c on e.enquiry_course = c.course_id where e.enquiry_assigned_employee = 0 and e.enquiry_deleted = 0 $where order by e.enquiry_id desc ");
			$stmt->execute();
			$row_count = $stmt->fetchAll();
			return $row_count;
		
	 }
	 
	 // Pagination Calculation
	 public function pagination($rows,$page)
	 {
				   $total = $page * $rows;
				   $start = $total - $rows;
				   $limit_stmt =" limit $start,$rows";
				   return $limit_stmt;
	 }
	 
	 public function chart_data($select,$where=null)
	 {
		 try{
			$stmt = $this->db->prepare($select);
			$stmt->execute($where);
			$res = $stmt->fetchAll();
			return $res;
		 }
		 catch(Exception $e)
		 {
			 echo $e->getMessage();
			 die;
		 }

	 }
	 public function getreportdata($select,$where=null)
	 {
			$stmt = $this->db->prepare($select);
			$stmt->execute($where);
			$res = $stmt->fetchAll();
			return $res;
	 }
	 
	 public function apienquiry_insert($tables,$keys,$values,$value_keys)
	{
				
		try {  
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->beginTransaction();
				$stmt = $this->db->prepare("INSERT INTO $tables ($keys)  VALUES ($value_keys)");
				$res=$stmt->execute($values);
				$this->db->commit();
			   return true;
		}
		catch (Exception $e) 
		{
			 $this->db->rollBack();
			 return false;
		}
		
	}
	
	// Update JD Keyskills
	
	public function delete_records($tablename,$where,$values)
	{
		try {  
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->beginTransaction();
			$stmt = $this->db->prepare("DELETE from $tablename where $where");
			$res=$stmt->execute($values);
			$this->db->commit();
			return true;
		}
		catch (Exception $e) 
		{
			$this->db->rollBack();
			return false;
		}
	}
        
        public function generateRandomString($length = 36) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}

$database = new Database();
$db =& $database;

// ...some PHP code for database "my_db"...



?>