<?PHP

class View {

    protected $fmenu;
    protected $mainmenu;
    protected $variables = array();
    protected $base_site_url;

    function __construct($fmenu_det = null) {
        if (!empty($fmenu_det))
            $this->fmenu = $fmenu_det;

        // ob_start("rmspace");
    }

    public function setsiteurl($basesiteurl = null) {
        if (!empty($basesiteurl))
            $this->base_site_url = $basesiteurl;
        else
            $this->base_site_url = BASE_URL;
    }

    public function set($name, $value) {
        $this->variables[$name] = $value;
    }

    /*
      -- home page
     */

    public function render($view_name, $data = null) {

        if ($data != null)
            extract($data);
        if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $view_name . '.php')) {
            include (ROOT . DS . 'application' . DS . 'views' . DS . $view_name . '.php');
        }
    }

  

  

}

?>