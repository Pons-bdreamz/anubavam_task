<?php

/* Create URL Related helper functions */


  if ( ! function_exists('redirect'))
{
	function redirect($uri = '', $method = 'location', $http_response_code = 302)
	{
		        if(BASE_URL === $uri)
                    $url = BASE_URL;
                else
                    $url = BASE_URL."/".$uri;
              
		switch($method)
		{
			case 'refresh'	: header("Refresh:0;url=".$url);
				break;
			default	: header("Location: ".$url, FALSE, $http_response_code);
				break;
		}
		exit;
	}
}


?>