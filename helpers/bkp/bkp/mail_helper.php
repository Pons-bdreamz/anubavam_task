<?php

/* Mail Functions */
define('SUP_MAILID', 'support@flikza.com');
$sitename = SITE_NAME;
define('MAIL','elastic');
//define('MAIL','gsuite');



/* Business Claim listing mail */

function bussclaimmail($mailary) {
    $to_address = $mailary[0];
    $buss_name = $mailary[1];
    $buss_url = BASE_URL . $mailary[2];
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
<tr><td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
<p>Hi,</p><p>We\'ve received your claim request.</p><p>Your Email ID : ' . $to_address . '</p><p>Business : ' . $buss_name . '</p>
<p>Business URL - <a href="' . $buss_url . '">' . $buss_url . '</a></p>
<p>Our team will verify your claim and respond as soon as possible</p>
<br/>Best Wishes<br/>Regards,<br/>Team Flikza.com<br/>Email     :' . SUP_MAILID . ' <br/></td></tr></table>';
    $message = constructtemplate($mailbodycontent);
    $subject = "Claiming Request Initiated - Flikza.com";
    $mail = sendusermail($to_address, "", $subject, $message);
    return $mail;
}

/* Business Report an Error mail */

function bussreporterrormail($mailary) {
    $to_address = $mailary[0];
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0"><tr>
<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
<p>Hi,</p><p>Thanks for your time to correct one of our business listing.</p>
<p>Our team will verify your suggested informations as soon as possible</p>
<br/>
Best Wishes<br/>
Regards,<br/>
Team ' . SITE_NAME . '<br/>
Email     : ' . SUP_MAILID . '<br/></td></tr></table>';
    $message = constructtemplate($mailbodycontent);
    $subject = "Error Notification Received - Flikza.com";
    $mail = sendusermail($to_address, "", $subject, $message);
    return $mail;
}

/* Business Send Phone or Email mail */

function bussphonemail($mailary, $userary) {
    $to_address = $mailary['to'];
    $buss_name = $mailary['bname'];
    $address = $mailary['baddr'];
    $contact = $mailary['bcont'];
    $category = $mailary['bcat'];
    $subcategory = $mailary['bscat'];
    $bemail = $mailary['bemail'];
    $buss_loc = $mailary['bloc'];
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- detail --><tr><td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
<p>Hi,</p><p>As per your request, </p><p>Business Name : ' . $buss_name . '</p><p>Address : ' . $address . '</p>
<p>Contact No : ' . $contact . '</p><p>Email : ' . $bemail . '</p>';
    if (!empty($category))
        $mailbodycontent .= '<p>Category : ' . $category . '</p>';
    if (!empty($subcategory))
        $mailbodycontent .= '<p>Sub Category : ' . $subcategory . '</p>';
    $mailbodycontent .= '<br/>Best Wishes<br/>Regards,<br/>Team ' . SITE_NAME . '<br/>Email     : ' . SUP_MAILID . '<br/></td></tr><!-- end detail --></table>';

    $message = constructtemplate($mailbodycontent);
    if (!empty($buss_loc))
        $subject = $buss_name . ", " . $buss_loc . " - Information from Flikza.com";
    else
        $subject = $buss_name . " - Information from Flikza.com";

    $mail = sendusermail($to_address, "", $subject, $message);
    /* if business email exists send the userdetils to their mail id */
    if (isset($userary['to']) && !empty($userary['to'])) {
        $bmailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- detail --><tr><td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
<p>Hi,</p><p>A user of flikza.com saved your business details. </p><p>Name : ' . $userary['name'] . '</p>
<p>Contact No : ' . $userary['phone'] . '</p><p>Email : ' . $userary['email'] . '</p><br/>Best Wishes<br/>Regards,<br/>Team ' . SITE_NAME . '<br/>Email     : ' . SUP_MAILID . '<br/></td></tr><!-- end detail --></table>';
        $bmessage = constructtemplate($bmailbodycontent);
        $subject = $userary['name'] . " saved your business information - Flikza.com";
        $mail = sendusermail($userary['to'], "", $subject, $bmessage);
    }

    return true;
}

/* Business - Get Quote Mail send to the corresponding business */

function sendenqquote($mailary) {
    $to_address = $mailary['to'];
    $name = $mailary['ename'];
    $email = $mailary['email'];
    $phone = $mailary['econt'];
    $city = $mailary['ecity'];
    $area = $mailary['earea'];
    $tag = $mailary['einterested'];
    $comments = $mailary['ecomm'];
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
    <!-- detail --> <tr> <td style="font-family:Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;"> 
    <p>Hi,</p> <p>We had got a quote request in <a href="https://www.flikza.com">Flikza.com</a> 
    from a potential customer.</p> <p>Name : ' . $name . '</p> <p>Contact No : ' . $phone . '</p> 
    <p>Email ID : ' . $email . '</p> <p>City : ' . $city . '</p> <p>Interested Area : ' . $area . '</p> 
    <p>Service Looking For : ' . $tag . '</p> 
    <p>Comments : ' . $comments . '</p> 
    <p>Contact the customer quickly for better conversion.</p>  <br/> Best Wishes<br/>
    Regards,<br/> Team ' . SITE_NAME . '<br/> Email     : ' . SUP_MAILID . '<br/> </td> </tr> <!-- end detail --> </table>';
    $message = constructtemplate($mailbodycontent);
    $subject = "New Quote Request from " . $name . "[" . $phone . "]";
    $mail = sendusermail($to_address, "", $subject, $message);
    return $mail;
}

/* New User Welcome Mail with account activation link */

function senduserwelcome($mailary) {
    global $sitename;
    $activatation_link = BASE_URL . "/user/emailverification/" . $mailary[2];
    $to_address = $mailary[0];
    $user_name = $mailary[1];
    $subject = "Welcome to Flikza.com | Activate Your Account";
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">

														<!-- detail -->
														<tr>
															<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
																<p>Dear ' . $user_name . ',</p>
																<p>Thanks for creating account on <a href="https://www.flikza.com">flikza.com</a>,</p>
<p>You have created your account on <a href="https://www.flikza.com">flikza.com</a> by specified following details:</p>
<p>Email               : ' . $to_address . '</p>
<p>' . $user_name . ', your account on flikza is not confirmed yet, to access flikza services and benefits you need to confirm your account.</p>
<p>In order to confirm or activate your account, please click the following link</p>
<a href="' . $activatation_link . '">' . $activatation_link . '</a>
<br/>
Or Click this button<br/>
<!-- button -->

																			<table width="140" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#cf4747" class="textbutton" style="border-radius:5px;">
																				<tr>
																					<td width="5"></td>
																					<td height="35" align="center" style="font-family: Open Sans, Arial, sans-serif; font-size:15px; color:#ffffff;line-height:24px;">
																						<a href="' . $activatation_link . '" style="color:#FFFFFF;" target="_blank">Activate Account</a>
																					</td>
																					<td width="5"></td>
																				</tr>
																			</table>

																			<!-- end button -->
																			<br/><br/>
<p>After the confirmation of account, You can update your profile details with flikza and search millions of businesses.</p>

Regards,<br/>
Team Flikza.com<br/>
Email     : support@flikza.com<br/>
															</td>
														</tr>
														<!-- end detail -->
													</table>';
    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

/* After User Account Activation Success Mail */

function senduseractivation($mailary) {
    global $sitename;
    $to_address = $mailary[0];
    $user_name = $mailary[1];
    $subject = "Activation Success | Welcome to Flikza.com";
    $loginlink = BASE_URL;
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">

														<!-- detail -->
														<tr>
															<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
																<p>Dear ' . $user_name . ',</p>
																<p>Thanks for creating account on <a href="https://www.flikza.com">flikza.com</a>,</p>
<p>You have created your account on <a href="https://www.flikza.com">flikza.com</a> by specified following details:</p>
<p>Email               : ' . $to_address . '</p>
<p>' . $user_name . ', with this e-mail address and the password you have selected, you can now go to <a href="https://www.flikza.com">flikza.com</a> and log into your private account.</p>
<!-- button -->

																			<table width="140" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#cf4747" class="textbutton" style="border-radius:5px;">
																				<tr>
																					<td width="5"></td>
																					<td height="35" align="center" style="font-family: Open Sans, Arial, sans-serif; font-size:15px; color:#ffffff;line-height:24px;">
																						<a href="' . $loginlink . '" style="color:#FFFFFF;" target="_blank">Login Now</a>
																					</td>
																					<td width="5"></td>
																				</tr>
																			</table>

																			<!-- end button -->
<br/><br/>
Best Wishes<br/>
Regards,<br/>
Team Flikza.com<br/>
Email     : support@flikza.com<br/>
															</td>
														</tr>
														<!-- end detail -->
													</table>';
    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

/* Send Enquiry and Quick Enquiry form content send to the corresponding users */

function senduserenquiry($mailary) {
    $to_address = $mailary['to'];
    $name = $mailary['ename'];
    $email = $mailary['email'];
    $phone = $mailary['econt'];
    $city = $mailary['ecity'];
    $area = $mailary['earea'];
    $tag = $mailary['einterested'];
    $comments = $mailary['ecomm'];
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">  '
            . '<!-- detail --> <tr> <td style="font-family:Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;'
            . 'line-height:24px;"> <p>Hi,</p> <p>We had got a new potential enquiry in <a href="https://www.flikza.com">'
            . 'Flikza.com</a> matching your profile.</p> <p>Name : ' . $name . '</p> <p>Contact No : ' . $phone . '</p> '
            . '<p>Email ID : ' . $email . '</p> <p>City : ' . $city . '</p> <p>Interested Area : ' . $area . '</p> '
            . '<p>Service Looking For : ' . $tag . '</p> <p>Comments : ' . $comments . '</p> <p>Contact the customer quickly for better conversion.</p>'
            . '  <br/> Best Wishes<br/> Regards,<br/> Team Flikza.com<br/> Email     : ' . SUP_MAILID . '<br/> '
            . '</td> </tr> <!-- end detail --> </table>';
    $message = constructtemplate($mailbodycontent);
    $subject = 'Flikza.com - New Enquiry by ' . $name . ' [' . $phone . '] for [' . $tag . ']';

    foreach ($to_address as $key => $val) {
        $mail = sendusermail($val, "", $subject, $message);
    }
    return true;
}

/* Password Reset Mail */

function sendfrgtpwd($mailary) {
    global $sitename;
    $to_address = $mailary['to'];
    $user_name = $mailary['username'];
    $subject = "Reset your Flikza.com Password";
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">

														<!-- detail -->
														<tr>
															<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
																<p>Hi,</p>
																<p>To initiate the password reset process for your
' . $to_address . ' Flikza Account, click the link below:</p>
																<p><a href="' . $mailary['url'] . '">' . $mailary['url'] . '</a></p>
<p>If clicking the link above doesn\'t work, please copy and paste the URL in a
new browser window instead.</p>
<p>If you\'ve received this mail in error, it\'s likely that another user entered your email address by mistake while trying to reset a password. If you didn\'t initiate the request, you don\'t need to take any further action and can safely disregard this email.</p>

<br/>
Best Wishes<br/>
Regards,<br/>
Team Flikza.com<br/>
Email     : support@flikza.com<br/>
															</td>
														</tr>
														<!-- end detail -->
													</table>';
    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

/* After business verification, sending verification mail to business user with password */

function sendbususrcreated($mailary) {
    global $sitename;
    $loginlink = BASE_URL;
    $to_address = $mailary["email"];
    $user_name = $mailary["contact_name"];
    $subject = "Flikza.com verified and activated your business profile | Your Login Details";
    if (isset($mailary['password']))
        $login_info = '<p>Username :' . $to_address . ' </p><p>Password :' . $mailary['password'] . '</p>';
    else {
        $login_info = "";
        return false;
    }
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">

														<!-- detail -->
														<tr>
															<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
																<p>Hi,</p>
																<p>Thank you for registering with us. We are excited to have you here. Your business has been verified.</p>
																<p>Your Login Informations: </p>
' . $login_info . '

<!-- button -->

																			<table width="140" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#cf4747" class="textbutton" style="border-radius:5px;">
																				<tr>
																					<td width="5"></td>
																					<td height="35" align="center" style="font-family: Open Sans, Arial, sans-serif; font-size:15px; color:#ffffff;line-height:24px;">
																						<a href="' . $loginlink . '" style="color:#FFFFFF;" target="_blank">Login Now</a>
																					</td>
																					<td width="5"></td>
																				</tr>
																			</table>

																			<!-- end button -->
																			<br/>
<p>Benefits of registering with us:</p>
<ul>
	<li>Login and enhance your profile strength and get more clicks and views by adding engaging content.</li>
	<li>Present and visible where consumers search. </li>
	<li>Engage, encourage, and respond to consumers who have opinions about your business.</li>
</ul>
<br/>
Best Wishes<br/>
Regards,<br/>
Team Flikza.com<br/>
Email     : support@flikza.com<br/>
															</td>
														</tr>
														<!-- end detail -->
													</table>';
    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

/* After business verification, sending Welcome mail to business user */

function sendbususrwelcome($mailary) {
    global $sitename;
    $loginlink = BASE_URL;
    $to_address = $mailary["email"];
    $user_name = $mailary["contact_name"];
    $subject = "Welcome to Flikza.com | Grow your business with Flikza.com";
    $mailbodycontent = '<table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">

														<!-- detail -->
														<tr>
															<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:14px; color:#707575;line-height:24px;">
																<p>Hi,</p>
																<p>Thank you for Listing with <a href="https://www.flikza.com">Flikza.com</a>, We are proud of welcoming you as a service provider to Flikza Family.</p>
																<p><a href="https://www.flikza.com">Flikza.com</a> is an exclusive website for service providers with more than 10,000+ listings and more than 15,000 training enquires per year. Now, you can access to all the training enquiries just a click away. This service is totally FREE as of now. 
We are with you to make your business success.</p>
<p>You have created your account on <a href="https://www.flikza.com">flikza.com</a> by specified following details:</p>
<p>Email               : ' . $to_address . '</p>
<p>' . $user_name . ', with this e-mail address and the password you have selected, you can now go to <a href="https://www.flikza.com">flikza.com</a> and log into your business account.</p>
<!-- button -->

																			<table width="140" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#cf4747" class="textbutton" style="border-radius:5px;">
																				<tr>
																					<td width="5"></td>
																					<td height="35" align="center" style="font-family: Open Sans, Arial, sans-serif; font-size:15px; color:#ffffff;line-height:24px;">
																						<a href="'. $loginlink .'" style="color:#FFFFFF;" target="_blank">Login Now</a>
																					</td>
																					<td width="5"></td>
																				</tr>
																			</table>

																			<!-- end button -->
<br/><br/>
Best Wishes<br/>
Regards,<br/>
Team Flikza.com<br/>
Email     : support@flikza.com<br/>
															</td>
														</tr>
														<!-- end detail -->
													</table>';
    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

/* Remainder Mail for Business user to update details */

function sendremainder($mailary) {
    global $sitename;
    $loginlink = BASE_URL;
    $to_address = $mailary["email"];
    $user_name = $mailary["contact_name"];
    $subject = "Verify your business";
    $mailbodycontent = '<table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr>
                        <td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:25px; color:#3b3b3b; font-weight: bold; letter-spacing:2px;">Keep your ' . $sitename . ' account updated</td>
                      </tr>
                      <tr>
                        <td align="center">
                          <table width="25" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                              <td height="20" style="border-bottom:2px solid #fffbb3;"></td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                      <tr>
                        <td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">Hi <b>' . $user_name . '</b>, <br/>Login and enhance your profile strength and get more clicks and views by updating the below details.</td>
                      </tr>
                      <tr>
                      	<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
                        	' . $mailary["bus_details"] . '                        		
                        </td>
                      </tr>
                      <tr>
						  <td height="40"></td>
					  </tr>
					  <tr>
						<td align="center">
							<table align="center" bgcolor="#3b3b3b" border="0" cellspacing="0" cellpadding="0" style=" border-radius:30px; box-shadow: 0px 1px 0px #d4d2d2;">
							  <tbody><tr>
								<td height="55" align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:18px; color:#FFFFFF; font-weight: bold;padding-left: 25px;padding-right: 25px;">
								  <a href="' . $loginlink . '" style="color:#fffbb3;text-decoration:none;">Login Now</a>
								</td>
							  </tr>
							</tbody></table>
						</td>
					  </tr>
					</tbody></table>';

    $message = constructtemplate($mailbodycontent);
    $mail = sendusermail($to_address, $user_name, $subject, $message);
    return $mail;
}

function constructtemplate($mailbodycontent) {
    global $sitename;
    $imgpath = BASE_URL . '/theme/images';
    $template = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<title>Forget Password - Flikza</title>
	<style type="text/css">
.ReadMsgBody { width: 100%; background-color: #ffffff; }
.ExternalClass { width: 100%; background-color: #ffffff; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
html { width: 100%; }
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
table { border-spacing: 0; border-collapse: collapse; }
img { display: block !important; }
table td { border-collapse: collapse; }
.yshortcuts a { border-bottom: none !important; }
a { color: #cf4747; text-decoration: none; }
.textbutton a { font-family: \'open sans\', Calibri, arial, sans-serif !important; color: #ffffff !important; }
.textlink a { font-family: \'open sans\', Calibri, arial, sans-serif !important; color: #7f8c8d !important; }

table[class="courseinfo"] td{ 
		padding:7px;
	}
	/* provide some minimal visual accomodation for IE8 and below */
table[class="courseinfo"] tr{
		background: #FFFFFF;
	}
	/*  Define the background color for all the ODD background rows  */
table[class="courseinfo"] tr:nth-child(odd){ 
		background: #FFFFFF;
	}
	/*  Define the background color for all the EVEN background rows  */
table[class="courseinfo"] tr:nth-child(even){
		background: rgb(238, 238, 238);
	}

 @media only screen and (max-width: 640px) {
body { width: auto !important; }
table[class="table600"] { width: 450px !important; text-align: center !important; }
table[class="table-inner"] { width: 90% !important; }
table[class="table2-2"] { width: 47% !important; text-align: left !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 29% !important; }
table[class="table3-1"] { width: 64% !important; text-align: left !important; }
/* Image */
img[class="img1"] { width: 100% !important; height: auto !important; }

}
 @media only screen and (max-width: 479px) {
body { width: auto !important; }
table[class="table600"] { width: 290px !important; }
table[class="table-inner"] { width: 82% !important; }
table[class="table2-2"] { width: 100% !important; text-align: center !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 100% !important; }
table[class="table3-1"] { width: 100% !important; text-align: center !important; }
/* image */
img[class="img1"] { width: 100% !important; }
}
</style>
</head>

<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ecf0f1" style="">
		<tr>
			<td height="25"></td>
		</tr>
		<tr>
			<td style="font-family: Open Sans, Calibri, Arial, sans-serif; font-size:13px; color:#7f8c8d;line-height:34px;" align="center">
				Welcome to 
				<a href="https://www.flikza.com" target="_blank">Flikza.com</a>
			</td>
		</tr>
		<tr>
			<td height="5" style="font-size:0px; line-height:5px;">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table bgcolor="#f6f6f6" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="table600" style="border-left:1px solid #dddbdb;border-right:1px solid #dddbdb;border-top:1px solid #dddbdb;">
					<tr>
						<td>
							<table width="600" class="table600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-left: 5px solid #fbfbfb;border-right: 5px solid #fbfbfb;border-top: 5px solid #fbfbfb;">
								<tr>
									<td style="line-height:0px;">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<!-- 1/1 image -->
		<tr>
			<td>
				<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="table600" style="border-left:1px solid #dddbdb;border-right:1px solid #dddbdb;">
					<tr>
						<td>
							<table class="table600" style="border-left: 5px solid #fbfbfb;border-right: 5px solid #fbfbfb;" bgcolor="#f6f6f6" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
									<td>
										<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
											<tr>
												<td style="border:3px solid #ffffff;" bgcolor="#FFFFFF" height="30">
													' . $mailbodycontent . '
												</td>
											</tr>
											<tr>
												<td align="center" style="line-height:0px;">
													
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- end 1/1 image -->

		<!-- CTA bar-->
		<tr>
			<td>
				<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="table600" style="border-left:1px solid #dddbdb;border-right:1px solid #dddbdb;">
					<tr>
						<td>
							<table class="table600" style="border-left: 5px solid #fbfbfb;border-right: 5px solid #fbfbfb;" bgcolor="#f6f6f6" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								
								<tr>
									<td>
										<table class="table-inner" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
											<tr>
												<td height="35" style="border-bottom:1px solid #e3e3df;"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- end CTA bar -->
		<tr>
			<td>
				<table class="table600" width="550" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td height="15"></td>
					</tr>
					<tr>
						<td>
							<table width="600" border="0" align="left" cellpadding="0" cellspacing="0" class="table3-3">
								<tr>
									<td style="font-family: Open Sans, Arial, sans-serif; font-size:12px; color:#7f8c8d;line-height:24px;">
										Copyright ©
										<span style="color:#cf4747; font-weight: bold;">Flikza.com</span>
										2017, All rights reserved.
									</td>
								</tr>
							</table>



							<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
								<tr>
									<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
										<p style="padding-left: 24px;">&nbsp;</p>
									</td>
								</tr>
							</table>

							<!--End Space-->

						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="55"></td>
		</tr>
	</table>
</body>
</html>';
    return $template;
}

function sendusermail($to_address, $user_name, $subject, $message) {
    if (MAIL == "elastic") {
        $res = "";
        $data = "username=" . urlencode("admflikza@gmail.com");
        $data .= "&api_key=" . urlencode("9fd8b097-0506-403d-ba67-e935424279b0");
        $data .= "&from=" . urlencode("donotreply@flikza.com");
        $data .= "&from_name=" . urlencode("Flikza.com");
        $data .= "&to=" . urlencode($to_address);
        $data .= "&subject=" . urlencode($subject);
        if ($message)
            $data .= "&body_html=" . urlencode($message);

        $header = "POST /mailer/send HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
        $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

        if (!$fp)
            return "ERROR. Could not open connection";
        else {
            fputs($fp, $header . $data);
            while (!feof($fp)) {
                $res .= fread($fp, 1024);
            }
            fclose($fp);
        }
        return true;
    } else if (MAIL == "gsuite") {
        global $sitename;
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 25;
        $mail->Username = "donotreply@flikza.com";
        $mail->Password = "flikza@123";
        $mail->AddReplyTo('donotreply@flikza.com', 'Flikza');
		$mail->AddAddress($to_address, '');
		$mail->AddBCC("maniteamdev@gmail.com", "Mani Team");
        $mail->SetFrom('donotreply@flikza.com', 'Flikza');
        $mail->AddReplyTo('donotreply@flikza.com', 'Flikza');
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->IsHTML(true);
		$mail->Send();
		return true;
		/*
        if ($mail->Send())
            return true;
        else
            return false;
	 */
    }
}
?>

