
/*Normal User Related Js Starts*/
$(document).ready(function () {

    if ($.ui && $.ui.dialog && $.ui.dialog.prototype._allowInteraction) {
        var ui_dialog_interaction = $.ui.dialog.prototype._allowInteraction;
        $.ui.dialog.prototype._allowInteraction = function (e) {
            if ($(e.target).closest('.select2-dropdown').length)
                return true;
            return ui_dialog_interaction.apply(this, arguments);
        };
    }
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};

    /* Common select2 drop down box */
    $(".select2").select2();
    $(".select2").select2({tags: false});

    $(".form-control").tooltip({placement: 'top'});

    $("#all_cities_listing").select2({
        placeholder: "Select a City",
        allowClear: true,
        ajax: {

            url: "/search/allcities",
            dataType: 'json',

            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                var res = $.map(data.items, function (obj) {
                    obj.id = obj.city_id;
                    obj.text = obj.city_name;

                    return obj;
                });
                return {
                    results: res
                };

            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatRepo1,
        templateSelection: formatRepoSelection1
    });
    function formatRepo1(repo) {
        if (repo.loading)
            return repo.text;
        var markup = repo.city_name;
        return markup;
    }

    function formatRepoSelection1(repo) {
        if (repo.id == "") {
            return repo.text;
        } else {
            if (typeof repo.city_name != "undefined")
            {
                return repo.city_name || repo.id;
            } else {
                return repo.text || repo.id;
            }
        }

    }

    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    /* Category listing page js starts */
    $("ul.list-grp li").click(function ()
    {
        var target = $(this).attr('data-target');
        if (target != '') {
            if ($(this).hasClass("yellow-list")) {
                $(this).removeClass("yellow-list");
                $(this).addClass("yellow-list-active");
            } else {
                $(this).removeClass("yellow-list-active");
                $(this).addClass("yellow-list");
            }
        }
    });
    /* Category listing page js ends */

    $("#currlocation").click(function () {
        $("#locupdate").css("display", "block");
        $(".modal-backdrop").addClass("in");
    });

    /* Home page menu js starts */

    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI

    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\">Navigation</a>");

    // Adds menu-mobile class (for mobile toggle menu) before the normal menu
    // Mobile menu is hidden if width is more then 959px, but normal menu is displayed
    // Normal menu is hidden if width is below 959px, and jquery adds mobile menu
    // Done this way so it can be used with wordpress without any trouble

    $(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).siblings().children("ul").hide();
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });
    //If width is more than 943px dropdowns are displayed on hover	

    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 943) {
            $(this).children("ul").fadeToggle(150);
        }
    });
    //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

    /* Home page menu js ends */
});

/*Normal User Related Js Starts*/
var err_formname = "";
function uservalidFunction(e) {
    var ret_val = 1;
    var chkmsg = e.attr('valid-name');
    switch ($.trim(chkmsg)) {
        case 'email':
            var regex = /^[_a-z0-9A-Z-]+(\.[_a-z0-9A-Z-]+)*@([a-z0-9A-Z-]{3,100})+(\.[a-z0-9A-Z-]+)*(\.[a-zA-Z-]{2,4})$/;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val()))) {
                    ret_val *= usrfailCall(e, 'Please enter a valid email', 1);


                } else {
                    ret_val *= usrsuccessCall(e);
                }
            } else
            {
                ret_val *= usrfailCall(e, 'Please enter a valid email', 1);

            }
            break;
        case 'fullname':
            var regex = /^([A-Za-z.\s]){3,200}$/;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val())) && e.closest('.well').css('display') !== 'none' && e.closest('.well').css(
                        'display') !== 'none') {
                    ret_val *= usrfailCall(e, 'Please enter a valid name', 1);
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            } else
            {

                ret_val *= usrfailCall(e, 'Please enter a name', 1);

            }
            break;
        case 'password':
            var regex = /^[a-zA-Z0-9-_@#$%&*.]{8,30}$/;
            if (!regex.test($.trim(e.val())) && !(e.prop('disabled'))) {
                ret_val *= usrfailCall(e, 'Please enter a password with at least 8 characters', 1);
            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
        case 'required':
            if (e.val().length > 0) {
                ret_val *= usrsuccessCall(e);
            } else {
                ret_val *= usrfailCall(e, "Please enter the valid detail");
            }
            break;
        case 'address':

            var regex = /^([a-z0-9.,;:\-\/\{\}\[\]#&\(\)\'\"*\s+]{3,50000})$/i;
            if (!regex.test($.trim(e.val())) && e.closest('.well').css('display') !== 'none') {
                ret_val *= usrfailCall(e, "Please enter a valid address");
            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
        case 'mobile':
            var regex = /^[0-9\+\-\s]{8,16}$/;
            if (e.val().length > 0) {
                if (!regex.test($.trim(e.val()))) {
                    ret_val *= usrfailCall(e, 'Enter a valid mobile number');
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            } else {
                ret_val *= usrfailCall(e, 'Please enter a mobile number');

            }
            break;

        case 'title':
            var regex = /^([A-Za-z0-9-?!&\s.\(\)\/\\|\{\}\:]){3,200}$/i;
            if (!regex.test($.trim(e.val()))) {
                ret_val *= usrfailCall(e, "Please enter the valid detail");
            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
        case 'description':
            var regex = /^([a-z]{1}[a-z0-9.,?$;<>^?:+#\s.\_\-\+\/\{\}\[\]@%&\(\)\'\"*! \s+\u2019]{5,50000})$/i;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val()))) {
                    ret_val *= usrfailCall(e, 'Please enter the valid details', 1);
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            } else
            {

                ret_val *= usrfailCall(e, "Please enter the details");
            }

            break;
        case 'website':
            var regex = /^([www]{3})+(\.[A-Za-z0-9]{1,}[A-Za-z0-9-]{1,})+(\.[a-zA-z]{2,4})$/i;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val())))
                {
                    ret_val *= usrfailCall(e, 'Enter a valid website');
                } else
                {
                    ret_val *= usrsuccessCall(e);
                }
            }
            break;
        case 'select':
            if (e.find('option:selected').val() == '' || typeof (e.find("option:selected")) == 'undefined' || e.find('option:selected').val() == 'undefined') {
                ret_val *= usrfailCall(e, 'Please select an option');
            } else {
                if (e.find('option:selected').val() != '' && typeof (e.find("option:selected")) != 'undefined' && e.find('option:selected').val() != 'undefined') {
                    ret_val *= usrsuccessCall(e);
                } else {
                    ret_val *= usrfailCall(e, "Please select an option");
                }
            }
            break;
        case 'comment':
            var regex = /^([a-z0-9.,?$;<>^?:\_\-\+\=\/\{\}\[\]@#%&\(\)\'\"*! \s+]{3,50000})$/i;
            if (!regex.test($.trim(e.val())) && e.closest('.well').css('display') !== 'none') {
                ret_val *= usrfailCall(e, "Enter a valid comment");
            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
        case 'retype_password':
            var org_pwd = $('#pwd').val();
            if ($.trim(e.val()) == '' || $.trim(e.val()) != $.trim(org_pwd))
            {
                ret_val *= usrfailCall(e, 'Password and retype password doesn\'t match');
            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
            //for review saving in business details
        case 'divdescription':
            var regex = /^([a-z]{1}[a-z0-9.,?$;<>^?:+#\s.\_\-\+\/\{\}\[\]@%&\(\)\'\"*! \s+\u2019]{140,50000})$/i;
            if (!regex.test($.trim(e.text()))) {
                ret_val *= usrfailCall(e);

            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
            //for reply saving in business details 
        case 'replydescription':
            var regex = /^([a-z]{1}[a-z0-9.,?$;<>^?:+#\s.\_\-\+\/\{\}\[\]@%&\(\)\'\"*! \s+\u2019]{5,50000})$/i;
            if (!regex.test($.trim(e.text()))) {
                ret_val *= usrfailCall(e);

            } else {
                ret_val *= usrsuccessCall(e);
            }
            break;
        case 'nonmandemail':
            var regex = /^[_a-z0-9A-Z-]+(\.[_a-z0-9A-Z-]+)*@([a-z0-9A-Z-]{3,100})+(\.[a-z0-9A-Z-]+)*(\.[a-zA-Z-]{2,4})$/;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val()))) {
                    ret_val *= usrfailCall(e, 'Enter a valid email', 1);
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            }
            break;
        case 'nonmandmobile':
            var regex = /^[0-9\+\-\s]{8,16}$/;
            if (e.val().length > 0) {
                if (!regex.test($.trim(e.val()))) {
                    ret_val *= usrfailCall(e, 'Enter a valid mobile number');
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            }
            break;
        case 'nonmandfullname':
            var regex = /^([A-Za-z.\s]){3,200}$/;
            if (e.val().length > 0)
            {
                if (!regex.test($.trim(e.val())) && e.closest('.well').css('display') !== 'none' && e.closest('.well').css(
                        'display') !== 'none') {
                    ret_val *= usrfailCall(e, 'Enter a valid name', 1);
                } else {
                    ret_val *= usrsuccessCall(e);
                }
            }

            break;
        case 'radio':
            var name = e.attr('name');
            if ($("input[type='radio'][name='" + name + "']:checked").length !== 0) {
                ret_val *= usrsuccessCall(
                        e);
            } else {
                ret_val *= usrfailCall(e, "Please select an option");
            }
            break;
        default:
            break;

    }

    return ret_val;
}

function usrsuccessCall(e) {
    if (e.attr('valid-name') != 'select')
    {
        $(e).tooltip({hide: "slideDown"});
    }
    var formname = $(e).closest("form").attr('id');

    if ($(e).is("select")) {
        $(e).next().find('.select2-selection').removeAttr('style');
    } else
        $(e).removeAttr('style');
    if (formname == "business-form")
    {
        $(e).closest(".form-group").find(".err-msg").hide();
    } else
    {
        $(e).parent().next(".err-msg").hide();
    }
    return 1;
}

function usrfailCall(e, msg) {
    usrerrorLog(e, msg);

    return 0;
}



function usrerrorLog(e, msg) {

    var req = 'Please Enter the Detail';
    var formname = $(e).closest("form").attr('id');
    if ($(e).is("select")) {
        $(e).next().find('.select2-selection').attr('style', 'border:#FF0000 1px solid;');
    } else
        $(e).attr('style', 'border:#FF0000 1px solid;');

    if (formname == "business-form") {
        if (msg) {
            $(e).closest(".form-group").find(".err-msg").remove();
            // $(e).closest('.form-group').children().find('.err-msg').remove();
            $(e).closest(".form-group").append("<div class='err-msg'>" + msg + '</div>');
        }
        $(e).closest(".form-group").children().last(".err-msg").show();
    } else {
        if (formname == "getquote_form" || formname == "enquiry_form" || formname == "sendphoneoremail" || formname == "getreport_form" || formname == "getownlist_form" || formname == "quick_enquiry_form")
        {
            if (msg)
            {
                $(e).parent().after("<div class='err-msg'>" + msg + '</div>');
            } else
            {
                $(e).parent().after("<div class='err-msg'>Please Enter the valid input</div>");
            }

            $(e).parent().next(".err-msg").show();
        } else
        {
            if (msg) {
                $(e).parent().next(".err-msg").remove();
                $(e).parent().after("<div class='err-msg'>" + msg + '</div>');
            }

            $(e).parent().next(".err-msg").show();
        }
    }
}


$(".user-submit").click(function () {
    if ($(this).attr('type') == 'submit')
    {
        err_formname = $(this).closest("form").attr('id');
        var formname = $(this).closest("form").attr('id');
        var window_form = 'usermodel';
        var form_url = $('#' + formname).attr('data-url');
        var btn_name = $(this);
        var btn_text = btn_name.html();
        btn_name.prop('disabled', true);
        btn_name.html('Sending...');
        $('#' + formname).find(".status-msg").html('Please ensure all fields are valid.');

        $('#' + formname).submit(function (e) {
            e.preventDefault();
        });
        // Javascript validation checking
        var val;
        var chkval = 1;

        $('#' + formname).find('.form-control').each(function () {
            val = uservalidFunction($(this));
            if (val == 0)
            {
                chkval = 0;
                btn_name.prop('disabled', false);
                btn_name.html(btn_text);
            } else
            {
                if (formname == 'business_login')
                    $(this).removeAttr('style');
                else
                    $(this).parent('div').removeAttr('style');
            }
        });

        if (chkval === 1)
        {
            if (formname == 'user_signup') {
                var form_values = $('#' + formname).serializeArray();
                form_values.splice(-1, 1);
                var hash = "";
                var pwdchrs = $('#pwd').val();
                for (var i = 0; i < pwdchrs.length; i++)
                {
                    hash = hash + pwdchrs.charCodeAt(i) + ",";
                }
                hash = hash.slice(0, -1);
                var hobj = {};
                hobj.name = "hash";
                hobj.value = hash;
                form_values.push(hobj)
            } else if (formname == 'user_login' || formname == 'business_login') {
                var form_values = $('#' + formname).serializeArray();
                form_values.splice(-1, 1);
                var hash = "";
                var pwdchrs = $('#inputuserpwd').val();
                for (var i = 0; i < pwdchrs.length; i++)
                {
                    hash = hash + pwdchrs.charCodeAt(i) + ",";
                }
                hash = hash.slice(0, -1);
                var hobj = {};
                hobj.name = "hash";
                hobj.value = hash;
                form_values.push(hobj);
            } else
            {
                var form_values = $('#' + formname).serialize();
            }
            $.ajax({

                url: form_url,
                data: form_values,
                type: 'POST',
                success: function (response) {
                    var result = eval("(" + response + ")");
                    if (result.success != true) {
                        btn_name.prop('disabled', false);
                        btn_name.html(btn_text);
                        $('#' + formname).find(".status-msg").html(result.msg);
                        $('#' + formname).find(".status-msg").parent('div').removeClass('hide');
                        $('#' + formname).find(".status-msg").parent('div').addClass('show');
                    } else
                    {
                        if ($("#" + formname).hasClass("no-modal") == true) {
                            $("#" + formname).replaceWith("<h4>" + result.msg + "</h4>");
                            setTimeout(function () {
                                window.location.replace(result.redurl);
                            }, 5000);
                        } else {
                            if (formname == 'user_login' || formname == 'business_login') {
                                $("#" + window_form).modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                location.reload();
                            } else {
                                $("#" + formname).replaceWith("<h4>" + result.msg + "</h4>");
                                setTimeout(function () {
                                    $("#" + window_form).modal('hide');
                                    $('body').removeClass('modal-open');
                                    $('.modal-backdrop').remove();
                                    location.reload();
                                }, 5000);
                            }
                        }
                    }
                }
            });

        }
    }
});

//reset password
$(".user_resetpwd").click(function () {
    if ($(this).attr('type') == 'submit')
    {
        err_formname = $(this).closest("form").attr('name');
        var formname = $(this).closest("form").attr('id');
        var form_url = $('#' + formname).attr('data-url');
        var btn_name = $(this);
        btn_name.prop('disabled', true);
        $('#' + formname).submit(function (e) {
            e.preventDefault();
        });
        // Javascript validation checking
        var chkval;
        $('#' + formname).find('.form-control').each(function () {
            var val = uservalidFunction($(this));
            if (val == 0)
            {
                chkval = 0;
                btn_name.prop('disabled', false);
                $(this).attr('style', "border:#FF0000 1px solid;");
            } else
            {
                chkval = 1;
                $(this).removeAttr('style');
            }
        });
        if (chkval === 1)
        {
            $(this).find(".status-msg").parent('div').removeClass('show');
            $(this).find(".status-msg").parent('div').addClass('hide');

            var form_values = $('#' + formname).serializeArray();

            form_values.splice(0, 2);
            var hash = "";
            var hashc = "";
            var pwdchrs = $('#pwd').val();
            var cpwdchrs = $('#usercpwd').val();
            for (var i = 0; i < pwdchrs.length; i++)
            {
                hash = hash + pwdchrs.charCodeAt(i) + ",";
                hashc = hashc + cpwdchrs.charCodeAt(i) + ",";
            }
            hash = hash.slice(0, -1);
            hashc = hashc.slice(0, -1);
            var hobj = {};
            hobj.name = "hash";
            hobj.value = hash;
            form_values.push(hobj)
            var hcobj = {};
            hcobj.name = "hashc";
            hcobj.value = hashc;
            form_values.push(hcobj);

            $.ajax({
                url: form_url,
                data: form_values,
                type: 'POST',
                success: function (result) {
                    var result = eval("(" + result + ")");
                    if (result.success != true) {
                        btn_name.prop('disabled', false);
                        $('#' + formname).find(".status-msg").html(result.msg);
                        $('#' + formname).find(".status-msg").parent('div').addClass('show');
                    } else
                    {
                        $('.resetmsg').append('<div class="col-xs-12"><div style="margin10px;" class="error"><h4>Success!</h4><br><div>' + result.msg + ' You can now use your new password to <a href="/user/login">login</a></div></div></div>');
                        setTimeout(function () {
                            $('.resetmsg').empty();
                            window.location = '/user/login';
                        }, 5000);
                    }
                }
            });
        } else
        {
            return false;
        }
    }
});

$('#loginprim').hide();
$('.btn-in').click(function (event) {
    $('#loginhome').hide();
    $('.hrrule').hide();
    $("#usermodel").find(".modal-title").text("Log in");
    $("#loginprim").css('left', '100px');
    $('#loginprim').show();
    event.preventDefault();
});
$('#signupprim').hide();
$('.btn-up').click(function (event) {
    $('#loginhome').hide();
    $('.hrrule').hide();
    $("#usermodel").find(".modal-title").text("Sign up");
    $("#signupprim").css('left', '100px');
    $('#signupprim').show();
    event.preventDefault();
});
$('#forgetpwdprim').hide();
$('.btn-fpwd').click(function (event) {
    var formname = $(this).parent().siblings('form').attr('id');
    resetform(formname);
    $('#loginprim').hide();
    $("#usermodel").find(".modal-title").text("Forgot Password?");
    $("#forgetpwdprim").css('left', '100px');
    $('#forgetpwdprim').show();
    event.preventDefault();
});

$('#usermodel').on('hidden.bs.modal', function (e) {
    $('#loginprim').hide();
    $('#forgetpwdprim').hide();
    $('#signupprim').hide();
    $("#usermodel").find(".modal-title").text("Sign up or log in to Flikza");
    $('#loginhome').show();
    $('.hrrule').show();
});

$('.form_border').click(function (event) {
    $(this).css("border", "#7323dc solid 1px");
})

/* To reset form values and error messages */
function resetform(formname)
{
    $('#' + formname).trigger("reset");
    $('#' + formname).find("input").removeAttr('style');
    $('#' + formname).find("input").parent('div').removeAttr('style');
    $('#' + formname).find(".err-msg").hide();
    $('#' + formname).find(".status-msg").parent('div').removeClass('show');
    $('#' + formname).find(".status-msg").parent('div').addClass('hide');
}

/*Normal User Related Js Ends*/
/* User Login Social Start*/
/*$('.form-control').on('focus blur', function (e) {
 $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
 }).trigger('blur');  */

/* User Login Social GMAIL User Login End */
/* 
 User Login Start */
$("#fb_btn").on('click', function (e) {
    checkLoginState();
});

var aid = "";
function checkLoginState(response) {
    FB.login(function (response) {
        // FB.getLoginStatus(function(response) {

        if (response.status === 'connected') {
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID
            aid = response.authResponse.userID;
            var social_data_param = {};
            console.log("user_id" + user_id);
            // FB.api('/me?fields=name,gender,picture,email,id,link,location,birthday', function(response) {
            FB.api('/me', function (responsssse) {
                console.log(responsssse);
                var social_data_param = {
                    name: response.name,
                    id: response.id,
                    email: response.email,
                    gender: response.gender,
                    link: response.link,
                    dob: response.birthday,
                    location: response.location
                };
                social_login_btn(social_data_param, 2);
            });

        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
        //  });
    }
    , {
        scope: 'email',
        return_scopes: true,
        enable_profile_selector: true
                //scope: 'email,user_location,user_birthday'
    });
}



/* Facebook User Login End */

function social_login_btn(social_response, type)
{
    console.log("Social Login BTn");
    $.ajax({
        url: 'user/social_registration',
        data: {
            'tid': type,
            'response': social_response
        },
        method: 'POST',
        success: function (response)
        {
            var result = eval('(' + response + ')');
            if (result.success != true) {
                $('.social_login').find(".social-msg").html(result.msg);
            } else
            {
                $("#usermodel").modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                window.location.replace(result.redurl);
            }

        }

    });
}
//Calculating no of stars in edit review page.	
$('.rating-container .fa').click(function (e) {

    $('.rating-container .fa').removeClass('fa-star scolor fa-star-o a');
    $(this).prevAll('.fa').addBack().addClass('fa-star a');
    $(this).nextAll('.fa').addClass('fa-star-o');
    var arr = document.getElementsByClassName("a");
    var len = arr.length;
    $('#arrlen').val(len);
    if (len == 1) {
        ratingtext = "Very Bad";
        color = "#F28B5A";
    } else if (len == 2) {
        ratingtext = "Bad";
        color = "#FFB235";
    } else if (len == 3) {
        ratingtext = "Good";
        color = "#FFD935";
    } else if (len == 4) {
        ratingtext = "Very Good";
        color = "#ADD633";
    } else if (len == 5) {
        ratingtext = "Excellent";
        color = "#A0C15A";
    } else {
    }
    $('.ratetext1').remove();
    $('.ratetext').append("<span class='ratetext1'style='color:" + color + "'>" + ratingtext + "</span>");
});

//saving routine of write review
$('#review-submit').on('click', function (e) {

    e.preventDefault();
    var id = $('#review-id').val();
    var text = $('#write-review-reply-textarea-id').html();
    var len1 = $('#arrlen').val();
    var name = $('#buss_name').val();
    var related_id = $('#related_id').attr('value');
    $('#review_msg').empty();
    $('#bor_red').css('border-color', '#cecece');
    $('.review_div').hide(500);

    dataparams = {"review-id": id, "review-desc": text, "arrlen": len1, "related_id": related_id};

    var val = 1;
    $('#textcontent').find('.form-control').each(function (e) {
        val = uservalidFunction($(this));
    });

    if (len1 && val)
    {
        if (val == 1)
        {
            $.ajax({
                url: '/user/review_save',
                type: 'POST',
                data: dataparams,
                success: function (data) {
                    var res = eval('(' + data + ')');
                    if (res.success == true)
                    {
                        $('.rating-container .fa').removeClass('fa-star-o a').removeClass('fa-star a').addClass('fa fa-star-o fa-2x scolor');
                        $('.ratetext1').empty();
                        $('#write-review-reply-textarea-id').text('');
                        $('.review_image').empty();
                        $('.reviewuploadedphotos').hide();
                        var review_id = res.data;

                        $.ajax({
                            type: 'POST',
                            url: '/user/export_review1',
                            data: {"id": id,
                                "name": name,
                                "rev_id": review_id},
                            success: function (response) {
                                var result = eval('(' + response + ')');

                                if (result.success == true)
                                {
                                    $('#related_id').attr('value', '');
                                    $('#arrlen').attr('value', "");
                                    $('#reviewsblock').after(result.data);
                                    $('.noreviews').remove();
                                    $('.writereviewcontainer').hide(500);
                                    $('.addreviewcontainer').show(500);

                                } else
                                {
                                    $('.review_div').show(500);
                                    $('#review_msg').append('Please Try again later');
                                }
                            }
                        })
                    } else
                    {
                        $('.review_div').show(500);
                        $('#review_msg').append(res.data);
                    }
                }
            });
        } else
        {
            $('.review_div').show(500);
            $('#review_msg').append(" Try again later.");

            return false;
        }
    } else
    {
        if (val)
        {
            $('#bor_red').css('border-color', 'red');
            $('.review_div').show(500);
            $('#review_msg').append("star rating is required.");
        } else if (len1)
        {
            $('.review_div').show(500);
            $('#review_msg').append("Review must have 140 characters and start with an alphabet.");
        } else
        {
            $('#bor_red').css('border-color', 'red');
            $('.review_div').show(500);
            $('#review_msg').append("star rating is required and review must have 140 characters and start with an alphabet");
        }
        return false;
    }
});

//reset of modal box when closing.
$(".modal-rem, .modal").on('hide.bs.modal', function (e) {
    var title = $(".modal-title").attr("default-value");
    $('.btn-chg').text('Save');
    $('.modal-body').find('.status-msg').parent('div').removeClass('show').addClass('hide');
    $('.btn-chg').prop('disabled', false);
    $('form').find('.form-control').attr('style', '');
    //$('form').trigger('reset');
    $("#enquiry_form, #getquote_form, #getreport_form, #getownlist_form, #sendphoneoremail").trigger('reset');
    $('.select2').val('').trigger('change');
    $('.modal-body').find('.error').remove();
    $('.error1').html('');
    $('.radiobutton').css('border', '');
    $('form').find(".err-msg").remove();
    $('form').find('.select2-selection').removeAttr('style');
    //Result modal for popups reset
    $('#modal_result_content').empty();

});


$(".form-validation").submit(function (e) {
    var fname = $(this).attr('id');
    err_formname = $(this).attr('name');
    var formname = $(this).attr('id');
    var val;
    var chkform = 1;

    $('.btn-chg').attr('disabled', true);
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (val == 0)
        {
            chkform = 0;
            e.preventDefault();
            $('.btn-chg').attr('disabled', false);
        }
    });
    if (fname == "business-form")
    {
        var mapping = $('#info_new').text();
        $('.maplang').val(mapping);
    }
    if (chkform == 1)
    {
        return true;
    } else
    {
        e.preventDefault();
        $('.btn-chg').attr('disabled', false);
        return false;
    }

});

//saving routine for quick enquiry in bbusiness details page.
$('.quick_enquiry_form').on('click', function (e) {
    var formname = $(this).closest("form").attr('id');
    err_formname = $(this).closest("form").attr('name');
    var business_id = $('#business_id').val();
    var source_url = $('#source_url').val();
    $('#' + formname).find(".err-msg").remove();

    $('.modal-body').find('.error').remove();
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var val;
    var errval = false;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });

    if (!errval)
    {
        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "business_id", value: business_id}, {name: "source_url", value: source_url});

        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });
        $.ajax
                ({
                    url: "/user/saveenquiry",
                    type: 'POST',
                    data: form_params,
                    success: function (response)
                    {
                        var result = eval('(' + response + ')');
                        if (result.success != true)
                        {
                            $('.btn-save').attr('disabled', false);
                            btn_name.html("Send Enquiry");
                        } else
                        {
                            $('.btn-save').attr('disabled', false);
                            btn_name.html("Send Enquiry");
                            $('#' + formname).trigger("reset");
                            $('#' + formname).hide();
                            $('#' + formname).before(result.msg);
                        }
                    }
                });
    } else
    {
        return false;
    }
});


//saving routine for send enquiry modal popup in business details page.
$('.btn_enquiry_form').on('click', function (e) {
    var formname = $(this).closest("form").attr('id');
    var err_formname = $(this).closest("form").attr('name');
    var form_url = $('#' + formname).attr('data-action');
    var business_id = $('#business_id').val();
    var category_id = $('#category_id').val();
    var source_url = $('#source_url').val();
    var window_form = $(this).closest("div.modal").attr('id');
    $('#' + formname).find(".err-msg").remove();
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var val;
    var errval = false;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });

    if (!errval)
    {

        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "business_id", value: business_id}, {name: "source_url", value: source_url}, {name: "categoryid", value: category_id});

        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });
        $.ajax
                ({
                    url: "/user/saveenquiry",
                    type: 'POST',
                    data: form_params,
                    success: function (response)
                    {
                        var result = eval('(' + response + ')');
                        if (result.success != true)
                        {
                            btn_name.html("Send Enquiry");
                            $('.btn-save').attr('disabled', false);
                        } else
                        {
                            $("#" + window_form).modal('hide');
                            btn_name.html("Send Enquiry");
                            $('.btn-save').attr('disabled', false);
                            $('#' + formname).trigger("reset");
                            $('#result_modal').modal('show');
                            $('#modal_result_content').append('<i class="fa fa-users" aria-hidden="true"></i><div>We will contact you shortly.</div>');
                            setTimeout(function () {
                                $('#result_modal').modal('hide');
                                $('#modal_result_content').empty();
                            }, 5000);
                        }
                    }
                });
    } else
    {
        return false;
    }
});

//saving routine for get quote modal popups in business details page.
$('.btn_quote_form').on('click', function (e) {
    var formname = $(this).closest("form").attr('id');
    var err_formname = $(this).closest("form").attr('name');
    var form_url = $('#' + formname).attr('data-action');
    //var business_id = $('#business_id').val();
    var category_id = $('#category_id').val();
    var source_url = $('#source_url').val();
    var window_form = $(this).closest("div.modal").attr('id');
    $('#' + formname).find(".err-msg").remove();
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var val;
    var errval = false;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });

    if (!errval)
    {

        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "source_url", value: source_url}, {name: "categoryid", value: category_id});

        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });
        $.ajax
                ({
                    url: "/user/saveenquiry",
                    type: 'POST',
                    data: form_params,
                    success: function (response)
                    {
                        var result = eval('(' + response + ')');
                        if (result.success != true)
                        {
                            btn_name.html("Send Enquiry");
                            $('.btn-save').attr('disabled', false);
                        } else
                        {
                            $("#" + window_form).modal('hide');
                            btn_name.html("Send Enquiry");
                            $('.btn-save').attr('disabled', false);
                            $('#' + formname).trigger("reset");
                            $('#result_modal').modal('show');
                            $('#modal_result_content').append('<i class="fa fa-users" aria-hidden="true"></i><div>We will contact you shortly.</div>');
                            setTimeout(function () {
                                $('#result_modal').modal('hide');
                                $('#modal_result_content').empty();
                            }, 5000);
                        }
                    }
                });
    } else
    {
        return false;
    }
});

/* Business Listing Page Start */
function filterlist(sorting) {
    var filter_data = {};
    var page_category_id = $("#page_category").val();
    var page_tag_id = $("#page_tag").val();
    var page_city_id = $("#page_city").val();
    var page_area_id = $("#page_area").val();
    var click_page = $('#page_no').val();
    var page_limits = $('#page_limits').val();
    filter_data['category_id'] = page_category_id;
    filter_data['tag_id'] = page_tag_id;
    filter_data['city_id'] = page_city_id;
    filter_data['area_id'] = page_area_id;
    filter_data['page_limit'] = page_limits;
    filter_data['cur_page'] = click_page;
    filter_data['order'] = sorting;
    if ($("input.category_filter_tags:checked").length > 0)
    {
        var filters = new Array();
        $.each($("input.category_filter_tags:checked"), function (e) {
            filters.push($(this).val());
        });
        filter_data['filters'] = filters;
    }
    $.ajax({
        type: 'post',
        data: filter_data,
        url: '/sitebusiness/filterbusinesslist',
        success: function (response)
        {
            var result = eval('(' + response + ')');
            $("#category_page_details").empty();
            $("#category_page_details").html(result.data);
            $('#page_no').val(result.page_no);
            $('#total_records').val(result.total_records);
            $('.cus_page').bootpag({total: result.total_records, page: result.page_no});

        }
    });

}

$(".category_filter_tags").on('change', function () {
    var sorting = $('.hiddensort').val();
    $('#page_no').val('1');
    $('#total_records').val(0);
    filterlist(sorting);
});

var cur_url = $(location).attr('href');
var total_records = $('#total_records').val();
$('.cus_page').bootpag({
    total: total_records,
    page: 1,
    maxVisible: 7,
    href: "#page{{number}}",
    wrapClass: 'page-click pagination'
}).on('page', function (event, num) {
    var click_pag = num;
    var filters = new Array();
    $.each($("input.category_filter_tags:checked"), function (e) {
        filters.push($(this).val());
    });
    var page_category_id = $("#page_category").val();
    var page_tag_id = $("#page_tag").val();
    var page_city_id = $("#page_city").val();
    var page_area_id = $("#page_area").val();
    var page_order = $("#hiddensort").val();
    $.ajax({
        type: 'post',
        url: '/sitebusiness/filterbusinesslist',
        data: {
            'category_id': page_category_id,
            'tag_id': page_tag_id,
            'city_id': page_city_id,
            'area_id': page_area_id,
            'order': page_order,
            'cur_page': click_pag,
            'page_limit': $('#page_limits').val(),
            'filters': filters
        },
        success: function (response)
        {


            var result = eval('(' + response + ')');
            if (result.success == true)
            {
                $("#category_page_details").empty();
                $("#category_page_details").html(result.data);
            } else
            {
                $("#category_page_details").empty();
                $("#category_page_details").html('<div>No Records Found</div>');
            }
            $('#page_no').val(result.page_no);
            $('#total_records').val(result.total_records);
            $('.cus_page').bootpag({total: result.total_records, page: result.page_no});
        }
    });
});

$(document).on('click', '.business_cmp', function (e)
{
    e.preventDefault();
    var button_name = $(this).html();
    if (button_name == "Add")
    {
        $(this).html("Remove");
        $(this).parent().prev().attr("data-attr", "selected");
    } else
    {
        $(this).html("Add");
        $(this).parent().prev().attr("data-attr", "not-selected");
    }
});
/* Business Listing Page End */
/* save routine for report-error modal box in business details page */
$('.btn_report_form').on('click', function (e) {

    var formname = $(this).closest("form").attr('id');
    err_formname = $(this).closest("form").attr('name');
    var window_form = $(this).closest("div.modal").attr('id');
    var business_id = $('#business_id').val();
    var source_url = $('#source_url').val();
    $('.error1').html('');
    $('.radiobutton').css('border', '');
    $('.modal-body').find('.error').remove();
    $('#' + formname).find(".err-msg").remove();
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var value = 1;
    var errval = false;

    if ($('[name="report_chk[]"]:checked').length == 0)
    {
        value = 0;
        $('.error1').html('Please select anyone checkbox');
    }
    if ($('[name="report_radio"]:checked').length == 0)
    {
        value = 0;
        $('.radiobutton').before('<div class="err-msg" style="display:block;">Please select any one option.</div>');
        $('.radiobutton').css('border', '1px solid red');
    }

    $('#' + formname).find('.form-control').each(function () {
        var val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });

    if (!errval && value)
    {
        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "business_id", value: business_id}, {name: "source_url", value: source_url});
        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });
        $.ajax
                ({
                    url: "/user/savereport",
                    type: 'POST',
                    data: form_params,
                    success: function (response)
                    {
                        var result = eval('(' + response + ')');
                        if (result.success != true)
                        {
                            btn_name.html("Report Error");
                            btn_name.attr('disabled', false);
                        } else
                        {
                            btn_name.html("Report Error");
                            btn_name.attr('disabled', false);
                            $("#" + window_form).modal('hide');
                            $('#' + formname).trigger("reset");
                            $('#result_modal').modal('show');
                            $('#modal_result_content').append('<i class="fa fa-exclamation-circle" aria-hidden="true"></i><div>Thank you for improving us.</div>');
                            setTimeout(function () {
                                $('#result_modal').modal('hide');
                                $('#modal_result_content').empty();
                            }, 5000);
                        }
                    }
                });
    } else
    {
        return false;
    }
});



/*	search box in all pages function	*/
$('.main-search-keyword').on('keyup', function (e)
{
    var keyword = $(this).val();
    var x = e.charCode || e.keyCode;
    if (keyword.length > 2)
    {
        $.ajax({
            url: '/search/searchcontents',
            data: {
                'c': keyword,
                's':$('#all_cities_listing').val()
            },
            type: 'POST',
            success: function (result)
            {
                var result = eval('(' + result + ')');
                if (result.success != true)
                {
                    $(".typeahead-result").addClass('hide');
                } else
                {
                    $(".typeahead-list").empty();
                    $.each(result.data, function (key, val)
                    {
                        $(".typeahead-list").append('<li><a href="/' + val.urlalias + '" data-group="courses" data-index="0"><span class="title">' + val.name + '</span></a></li>');
                    });
                    $.each(result.datas, function (key, val)
                    {
                        /*if(val.city_name != null && val.area_name != null)
                         $(".typeahead-list").append('<li><a href="/'+val.urlalias+'" data-group="courses" data-index="0"><span class="title">'+val.name+','+val.city_name+','+val.area_name+'</span></a></li>');
                         else if(val.city_name != null)
                         $(".typeahead-list").append('<li><a href="/'+val.urlalias+'" data-group="courses" data-index="0"><span class="title">'+val.name+','+val.city_name+'</span></a></li>');
                         else if(val.area_name != null)
                         $(".typeahead-list").append('<li><a href="/'+val.urlalias+'" data-group="courses" data-index="0"><span class="title">'+val.name+','+val.area_name+'</span></a></li>');
                         else */
                        $(".typeahead-list").append('<li><a href="/' + val.urlalias + '" data-group="courses" data-index="0"><span class="title">' + val.name + '</span></a></li>');
                    });
                    $(".typeahead-result").removeClass('hide');
                }
            }
        });
    } else
    {
        if (x == 8 || x == 46)
            $(".typeahead-result").addClass('hide');
    }
});


/* Business Compare Page Start */
$(document).on('click', '.compare_module', function (e) {
    e.preventDefault();
    var buss_id = $(this).attr('data-work-id');
    var area_id = $(this).attr('data-area-id');
    var city_name = $(this).attr('data-city');
    $('#main_id').val(buss_id);
    var page_category_id = $("#page_category").val();
    var page_tag_id = $("#page_tag").val();
    $.ajax({
        type: 'post',
        url: '/sitebusiness/busscompare',
        data: {
            'category_id': page_category_id,
            'tag_id': page_tag_id,
            'business_id': buss_id,
            'area_id': area_id,
            'city_name': city_name
        },
        success: function (response)
        {

            var result = eval('(' + response + ')');
            if (result.success == true)
            {
                $("#compare-business").empty();
                $("#compare-business").html(result.data);
            } else
            {
                $("#compare-business").empty();
                $("#compare-business").html('<li>No Records Found</li>');
            }
        }
    });
});

$(document).on('submit', '.compare-submit', function (e) {
    var compare_bus_id = "";
    var count = 0;
    $("li[data-attr='selected']").each(function () {
        compare_bus_id += ',' + $(this).val();
        count++;
    });
    $('#hid_bus_id').val(compare_bus_id);
    if (count < 1)
    {
        $('#hid_bus_msg').html("You have to select atleast 1 business.");
        return false;
    } else if (count > 4)
    {
        $('#hid_bus_msg').html("You can't select more than 4 business.");
        return false;
    } else
    {
        $(this).attr("action", "/sitebusiness/comparebusiness");
        $(this).submit();
    }

});
/* Business Compare Page End */

/*saving quote enquiry in search page */
$('.save-quote-enquiry').submit(function (e) {
    e.preventDefault();
    var val;
    $('#save-quote-enquiry').find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (val == 0)
        {
            return false;
        }
    });

    if (val == 1)
    {
        $('.quote-enquiry-submit').text("sending......");
        $('.quote-enquiry-submit').attr('disabled', true);
        var form_params = $('#save-quote-enquiry').serialize();
        $.ajax({
            url: '/user/saveenquiry',
            type: 'post',
            data: form_params,
            success: function (response)
            {
                var result = eval('(' + response + ')');
                if (result.success == true)
                {
                    $('.quote-enquiry-submit').html("<i class='fa fa-arrow-circle-o-right' aria-hidden='true'></i>Get Quote");
                    $('.quote-enquiry-submit').attr('disabled', false);
                    $("form[name='save-quote-enquiry']").trigger('reset');
                    $(".enquiry-err-msg").empty();
                    $(".enquiry-err-msg").html(result.msg);
                } else
                {
                    $('.quote-enquiry-submit').html("<i class='fa fa-arrow-circle-o-right' aria-hidden='true'></i>Get Quote");
                    $('.quote-enquiry-submit').attr('disabled', false);
                    $(".enquiry-err-msg").empty();
                    $(".enquiry-err-msg").html(result.msg);
                }

            }

        });
    }

});

/*ascending and descending for businesslisting */
$(".sortbyname").on('change', function () {
    var sorting = $(this).val();
    var sort = "";
    if (sorting == "desc")
    {
        $('.hiddensort').empty();
        $('.hiddensort').val("b.business_name desc");
        var sort = "b.business_name desc";
    } else if (sorting == "asc")
    {
        $('.hiddensort').empty();
        $('.hiddensort').val("b.business_name asc");
        var sort = "b.business_name asc";
    } else if (sorting == "star")
    {
        $('.hiddensort').empty();
        $('.hiddensort').val("rating desc");
        var sort = "rating desc";
    } else if (sorting == "score")
    {
        $('.hiddensort').empty();
        $('.hiddensort').val("s.business_score desc");
        var sort = "s.business_score desc";
    }
    filterlist(sort);
});


$('#contenttype').on('change', function () {
    var contenttype_id = $(this).val();
    categoryload(contenttype_id);
});
$('#category').on('change', function () {
    var category_id = $(this).val();
    subcategoryload(category_id);
});


function categoryload(cid)
{

    $.ajax({
        url: '/management/getcategorybycontenttype',
        data: {
            'content-type-id': cid
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            var option = "<option value=''>Select Category</option>";
            $.each(result.category, function (key, val)
            {
                option += "<option value=" + val.category_id + ">" + val.category_name + "</option>";
            });
            $("#category").html(option);
        }
    });
}

function subcategoryload(cid)
{
    $.ajax({
        url: '/management/getsubcategorybycategory',
        data: {
            'category-id': cid
        },
        type: 'POST',
        success: function (result) {

            var result = eval('(' + result + ')');
            console.log(result.subcategory);
            var option = "<option value=''>Select SubCategory</option>";
            $.each(result.subcategory, function (key, val)
            {
                option += "<option value=" + val.category_id + ">" + val.category_name + "</option>";
            });

            $("#subcategory").html(option);
        }
    });
}

/* contry data - ajax calling method */

function getcountry(couitems, selected_country_id)
{
    if (typeof (selected_country_id) == "undefined")
        selected_country_id = "";

    $.ajax({
        url: '/management/getcountry',
        type: 'POST',
        success: function (result) {

            var result = eval('(' + result + ')');
            if (result.success === true) {

                couitems.append('<option data-cname = "" value="">Select Country</option>');
                $.each(result.country, function (key, val) {
                    if (selected_country_id == val.cid)
                        couitems.append('<option data-cname = "' + (val.cname).toLowerCase() + '" value="' + val.cid + '" selected>' + val.cname + '</option>');
                    else
                        couitems.append('<option data-cname = "' + (val.cname).toLowerCase() + '" value="' + val.cid + '">' + val.cname + '</option>');
                });
            } else
            {

                console.log("Coutry Else");
            }
        }
    });
}
/* Country Drop down box data auto filling method */
if ($('form select').is('.listcountry')) {

    var couitems = $(".listcountry");
    var already_sel_country = $(".listcountry").attr('data-selected');
    couitems.empty();
    if (typeof (already_sel_country) != "undefined")
    {
        getcountry(couitems, already_sel_country);

    } else
    {
        getcountry(couitems);

    }

}
function stateload(cid, sid)
{
    if (typeof (sid) == "undefined")
    {
        sid = "";
    }
    $.ajax({
        url: '/management/getstatebycountry',
        data: {
            'country-id': cid
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            //console.log(result.state);
            var option = "<option value=''>Select State</option>";
            $.each(result.state, function (key, val)
            {
                if (sid == val.state_id)
                    option += "<option value=" + val.state_id + " selected>" + val.state_name + "</option>";
                else
                    option += "<option value=" + val.state_id + ">" + val.state_name + "</option>";
            });
            $("#area_state").html(option);
            $("#area_district").html('');
            $("#area_city").html('');
            $("#area_area").html('');
        }
    });
}

/* 
 * 
 * @param {type} cid  -  State id
 * @param {type} cityid - edit form automatically selected district id
 * @returns {undefined} 
 */
function districtload(cid, disid)
{
    if (typeof (disid) == "undefined")
    {
        disid = "";
    }

    $.ajax({
        url: '/management/getdistrictbystate',
        data: {
            'state-id': cid
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            var option = "<option value=''>Select District</option>";
            $.each(result.district, function (key, val)
            {
                /*if(disid == val.district_id)
                 option+="<option value="+val.district_id+" selected>"+val.district_name+"</option>";
                 else*/
                option += "<option value=" + val.district_id + ">" + val.district_name + "</option>";
            });

            $("#area_district").html(option);
            $("#area_city").html('');
            $("#area_area").html('');
        }
    });
}

function cityload(cid, cityid)
{
    if (typeof (cityid) == "undefined")
    {
        cityid = "";
    }
    $.ajax({
        url: '/management/getcitybystate',
        data: {
            'state-id': cid
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            var option = "<option value=''>Select City</option>";
            $.each(result.city, function (key, val)
            {
                if (cityid == val.city_id)
                    option += "<option value=" + val.city_id + " selected>" + val.city_name + "</option>";
                else
                    option += "<option value=" + val.city_id + ">" + val.city_name + "</option>";
            });
            $("#area_city").html(option);
            $("#area_area").html('');
        }
    });
}

function areaload(cid)
{
    $.ajax({
        url: '/management/getareabycity',
        data: {
            'city-id': cid
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            var option = "<option value=''>Select Area</option>";
            $.each(result.area, function (key, val)
            {
                option += "<option value=" + val.area_id + ">" + val.area_name + "</option>";
            });
            $("#area_area").html(option);
        }
    });
}
$('#area_country').on('change', function (event, param, sid) {
    if (typeof (param) == "undefined")
    {
        var country_id = $(this).val();
        stateload(country_id);
    } else if (typeof (sid) != "udefined")
    {
        var country_id = $(this).val();
        stateload(country_id, sid);
    }
});
$('#area_state').on('change', function (e, param, arid) {
    var form_id = $(this).closest('form').attr('id');
    var state_id = $(this).val();
    if ((form_id != 'stateform' && form_id != "country_form") && (state_id != "" && state_id != null))
    {
        if (typeof (param) == "undefined")
        {
            districtload(state_id);
        } else if (typeof (arid) != "udefined")
        {
            districtload(state_id, arid);
        }


    }
});
/* 
 *  District dependent drop down box loading methodology
 *  City load based on district id
 */
$('#area_district').on('change', function () {
    var form_id = $(this).closest('form').attr('id');
    var district_id = $(this).val();
    if (form_id == 'business-form')
    {
        cityload(district_id);
    }
});
$('#area_city').on('change', function () {
    var form_id = $(this).closest('form').attr('id');
    var city_id = $(this).val();

    if ((form_id != 'stateform' && form_id != "country_form" && form_id != "cityform" && form_id != "area_form") && typeof (city_id) != "undefined")
    {
        areaload(city_id);
    }
});


/*Own this list form submission*/
$('.getownlist_submit').on('click', function (e) {

    var formname = $(this).closest("form").attr('id');
    var err_formname = $(this).closest("form").attr('name');
    var form_url = $('#' + formname).attr('data-action');
    var window_form = $(this).closest("div.modal").attr('id');
    var business_id = $('#business_id').val();
    $('#' + formname).find(".err-msg").remove();

    $('.getownlist_form').submit(function (e) {
        e.preventDefault();
    });

    var val;
    var errval = false;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });
    if (!errval)
    {
        $(this).text("Sending....");
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "business_id", value: business_id});
        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });

        $.ajax({
            url: '/sitebusiness/ownbusinesssave',
            data: form_params,
            type: 'POST',
            success: function (response)
            {
                var result = eval('(' + response + ')');
                if (result.success == true)
                {

                    btn_name.attr('disabled', false);
                    btn_name.text("Claim Business");
                    $('#' + window_form).modal('hide');
                    $('#' + formname).trigger("reset");
                    $('#result_modal').modal('show');
                    $('#modal_result_content').append('<i class="fa fa-key" aria-hidden="true"></i><div>Thank you for your contribution.</div>');
                    setTimeout(function () {
                        $('#result_modal').modal('hide');
                        $('#modal_result_content').empty();
                    }, 5000);
                } else
                {
                    btn_name.text("Claim Business");
                    btn_name.attr('disabled', false);
                }
            }
        });
    } else
    {
        return false;
    }

});

/*for rating of star for business selected and related business in business details page
 Also used in individual review edit page*/
function star_rating(count)
{
    for (a = 1; a <= count; a++)
    {
        var review_average = $("#business_ds_star" + a).attr('value');
        var whole = parseInt(review_average);
        var decimal = review_average - whole;
        var $width = (decimal * 100) + "%";
        var business_star = "";
        for (i = 0; i < whole; i++)
        {
            business_star += '<i class="fa fa-star scolor "></i> ';
        }

        if (decimal)
        {
            business_star += '<i class="star star-under fa fa-star-o"><i class="star star-over fa fa-star" style="width:' + $width + '"></i></i> ';
            for (j = 5; j > whole + 1; j--)
            {
                business_star += '<i class="star star-under fa fa-star-o"> ';
            }
        } else
        {
            for (j = 5; j > whole; j--)
            {
                business_star += '<i class="star star-under fa fa-star-o"></i> ';
            }
        }
        $("#business_ds_star" + a).append(business_star);
    }

}

if ($('#business_ds_star1').hasClass("buss-reviews-stars"))
{
    var count = $("#business_rel_count").attr('value');
//Taking related business count and addinng additional two star rating in business details page.
    count = (parseInt(count, 10) + 2);
    star_rating(count);
}




/*user reviewdelete*/
$('.rev_delete').on('click', function () {

    var rev_del = $(this).attr('data-review-prop');
    $('.review-delete-modal').attr('data-review-details', rev_del);
    //$('.deletereviewtable').trigger();
});
$("#delete-review").on('click', function (e) {
    var re_id = $('.review-delete-modal').attr('data-review-details');
    $.ajax({
        url: '/user/reviewdelete',
        data: {
            'rev_id': re_id
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');

            if (result.success == true)
            {
                $("#reviewdelete").modal('hide');
                $("[data-review-prop=" + re_id + "]").closest('.reviewtable').remove();
                location.reload();
            }
        }
    });
});
$('#cancel-review').on('click', function () {
    $("#reviewdelete").modal('hide');
});
/*user reviewedit*/
$('.rev_edit').click(function () {
    var book_del = $(this).attr('data-review-bs-id');
    $('#review-pg-bs-id').val(book_del);
    var review_edit = $(this).attr('data-review-edit');
    $.ajax({
        url: '/user/reviewedit',
        data: {
            'rev_id': review_edit,

        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success)
            {
                $("#revieweditmodel").modal('show');
                $("#review_desc").text(result.revdata['rev_details']);
                $(".review-edit-id").val(result.revdata['rev_id']);
                $('.review-edit-photo').html('');
				 $('.review_image').html('');
                $.each(result.revdata['review_photos'], function (key, value) {
                    $('.review-edit-photo').append('<div class="col-sm-2"><img src="' + value['p_path'] + '" alt="" ><i class="fa fa-times review-delete" aria-hidden="true" data-review-content="' + value['pid'] + '" style="cursor:pointer;"></i></div');
                });
                $('#business_ds_star1').attr('value', result.revdata['rev_rating']);
                $('#arrlen').attr('value', result.revdata['rev_rating']);
                star_rating(1);

            }

        }
    });

});
/*user review update*/
$('.edit_btn').on('click', function () {

    var formname = $(this).closest("form").attr('id');

    err_formname = $(this).closest("form").attr('name');
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var val;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (val == 0)
        {
            return false;
        }
    });


    if (val == 1)
    {
        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();

        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });

        $.ajax({
            url: '/user/updatereview',
            data: form_params,
            type: 'POST',
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    location.reload();
                    $("#revieweditmodel").modal('hide');

                } else
                {
                    $('#err_msg').text(result.msg);
                }
            }
        });
    }
});
/*user bookmark Delete*/
$('.ibtn-delete').on('click', function () {

    var book_del = $(this).attr('data-delete-prop');
    console.log(book_del);
    $('.book-delete-modal').attr('data-delete-details', book_del);
});

$("#delete-bookmark").on('click', function (e) {
    console.log("adsfgngfdsfgb");
    var bookmark_id = $('.book-delete-modal').attr('data-delete-details');
    $.ajax({
        url: '/user/bookmarkdelete',
        data: {
            'book_id': bookmark_id
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');

            if (result.success == true)
            {

                $("#crossdelete").modal('hide');
                $("[data-delete-prop=" + bookmark_id + "]").closest('.new-rowbook').remove();
                location.reload();
            }

        }
    });
});
/*user review photo delete */
$(document).on('click', '.review-delete', function (e) {
    var photodel_id = $(this).attr('data-review-content');
    $.ajax({
        url: '/user/reviewdeletephoto',
        data: {
            'review_delete_id': photodel_id
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success)
            {
                $('[data-review-content=' + photodel_id + ']').parent('div').remove();
                $('[data-pgreview-content=' + photodel_id + ']').remove();

            }

        }
    });
});
$('#cancel-bookmark').on('click', function () {
    $("#crossdelete").modal('hide');
});

//photos uploading function for reviewedit addphotos
$('#rev_photo_edit_upload').fileupload({
    multipart: true,
    url: '/user/reviewphotoeditupload/',
    dataType: 'json',
    type: 'post',
    formData: {
        'businessid': $('#review-id').val()
    },
    acceptFileTypes: '/(\.|\/)(png|jpg|jpeg)$/i',

    add: function (e, data) {
        $('#upload_error1').text('');
        var error = false;
        if (data.originalFiles[0]['size'] > 2097152) {
            error = true;
        }
        if (error) {
            $("#upload_error1").text("File Size must be less than 2MB.");
        } else {
            data.submit();
        }

    },

    done: function (e, data)
    {
        if (data.result.success == true)
        {
            var related_id = data.result.id;
            inserted = $('#related_id').val();
            if (inserted) {
                $('#related_id').val(inserted + "," + related_id);
            } else {
                $('#related_id').val(related_id);
            }

            var path = '/theme/user_images' + data.result.path;
            $('.review_image').append('<div class="rev_img clo-lg-3"><img src="' + path + '" alt="" width="300" height="210"></div>');
        } else
        {
            $("body").removeClass("mask");
            //$("#upload_error").text(data.result.msg);
        }
    }
}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled')
        .bind('fileuploadsubmit', function (e, data) {
            data.formData = {
                'businessid': $('#review-pg-bs-id').val()
            };
        });

$(document).on('click', '.rev_upload_click', function (e)
{
    //alert("dckjscd"); 
    e.preventDefault();
    $("#rev_photo_edit_upload").trigger('click');
});

//Photos Uploading form business reviews page.		
$(document).on('click', '.upload_click1', function (e)
{
    e.preventDefault();
    $("#rev_photo_upload").trigger('click');
});

//photos uploading in reviews in business details click function		
$(document).on('click', '.upload_click', function (e)
{
    e.preventDefault();
    if ($(this).parent('div').hasClass('addphotosdiv')) {
        photos_value = true;     //for photos uploaded from only photos in business details page
        $('#photo_msg').empty();
        $('.photos_div').hide(500);
    } else {

        photos_value = false;    //for photos uploaded with reviews in business details page.
        $('#review_msg').empty();
        $('.review_div').hide(500);
    }

    $("#rev_photo_upload").trigger('click');
});
//photos uploading function for reviews adding in business details page.
$('#rev_photo_upload').fileupload({

    multipart: true,
    url: '/user/reviewphotoupload/',
    dataType: 'json',
    type: 'post',
    formData: {
        'businessid': $('#review-id').val(),
    },
    acceptFileTypes: '/(\.|\/)(png|jpg|jpeg)$/i',

    add: function (e, data) {
        $("body").addClass("mask");
        if (photos_value) {
            $('#photo_msg').empty();
            $('.photos_div').hide(500);
        } else {
            $('#review_msg').empty();
            $('.review_div').hide(500);
        }
        var error = false;
        if (data.originalFiles[0]['size'] > 2097152) {
            error = true;
        }
        if (error) {
            $("body").removeClass("mask");
            if (photos_value) {
                $('.photos_div').show(500);
                $('#photo_msg').append("File Size must be less than 2MB.");
            } else {
                $('.review_div').show(500);
                $('#review_msg').append("File Size must be less than 2MB.");
            }
        } else {
            data.submit();
        }

    },

    done: function (e, data) {
        if (data.result.success == true)
        {
            $("body").removeClass("mask");
            if (photos_value)
            {
                $('.photos_div').show(500);
                $('#photo_msg').text("Photo Uploaded successfully");
            } else
            {
                var related_id = data.result.id;
                inserted = $('#related_id').val();
                if (inserted) {
                    $('#related_id').val(inserted + "," + related_id);
                } else {
                    $('#related_id').val(related_id);
                }

                var path = '/theme/user_images' + data.result.path;
                $('.reviewuploadedphotos').show();
                $('.review_image').append('<li class="pull-left"><img src="' + path + '" style="width:100px;height:100px;" /></li>');
            }
        } else
        {
            $("body").removeClass("mask");
            if (photos_value) {
                $('.photos_div').show(500);
                $('#photo_msg').text(data.result.msg);
            } else {
                $('.review_div').show(500);
                $('#review_msg').text(data.result.msg);
            }
        }
    }
}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


$("#all_cities_listing").on('change', function (e) {
    if (e.target.value != "") {
        $.post("/search/chgcity",
                {
                    city: e.target.value
                }, function (data) {
            if (data != null)
            {
                console.log(data);
                location.href = data;
            }
        }
        );
    }
});
/*userstar*/
$(document).on('click', '.edit_star .fa', function (e) {
    $('.edit_star .fa').removeClass('fa-star-o a fa-star scolor star star-under');
    $(this).prevAll('.fa').addBack().addClass('fa-star a');
    $(this).nextAll('.fa').addClass('fa-star-o');
    var arr = document.getElementsByClassName("a");
    var len = arr.length;
    $('#arrlen').attr('value', len);

});

//upload logo click function for user_sidebar.php
$('#user_logo_upload').on('click', function (e) {
    e.preventDefault();
    $("#upload_logo_brwse").trigger('click');
});

//upload logo save function for user_sidebar.php	
$('#upload_logo_brwse').fileupload({
    multipart: true,
    url: '/user/profilelogoupload',
    dataType: 'json',
    type: 'post',
    acceptFileTypes: '/(\.|\/)(png|jpg|jpeg)$/i',

    add: function (e, data) {
        $('#upload_error').text('');
        var error = false;
        if (data.originalFiles[0]['size'] > 2097152) {
            error = true;
        }
        if (error) {
            $("#upload_error").text("File Size must be less than 2MB.");
        } else {
            data.submit();
        }

    },

    done: function (e, data) {
        $("body").addClass("mask");
        if (data.result.success == true)
        {
            var path = "/theme/user_images" + data.result.src;
            $('.profile-user-img').attr('src', path);
            location.reload();
        } else
        {
            $('#upload_error').html(data.result.data);
            $("body").removeClass("mask");
        }
    }
}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


/*Reply review delete in business details page */
$(document).on('click', '.rev_delete', function () {
    reply_id = $(this).attr('data-box-id');
    var reply_class = $(this);
    $.ajax({
        url: 'user/deletereply',
        data: {'reply_id': reply_id},
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success)
            {
                reply_class.parent('div').hide();
            } else
            {
                var err_txt = '<span style="color:red;">Please try again later</span>';
                $(err_txt).insertBefore(reply_class);
            }

        }
    })

});

/* showing of reply box for review in business details page click event */
$(document).on('click', '.fareply', function (e) {
    e.preventDefault();
    var currvalue = $(this).attr('value');
    var business_id = $(this).attr('data-main-content');
    var review_id = $(this).attr('data-child-id');
    var business_name = $(this).attr('data-buss-name');
    var user_name = $(this).attr('data-user-name');
    var f_letter = (user_name.charAt(0));
    var random = (Math.floor(Math.random() * 11) + 1);

    var component1 = '<div class="mainblock clearfix readreviews repliedtext" tabindex="-1" id="replay_box" data-rev-id="' + review_id + '"><div class="userimage pull-left hidden-xs"><span class="firstLetterColor' + random + ' nouserphoto">' + f_letter + '</span></div><form name="reply_form" id="reply_form" method="post" class="reply_form"><div class="reviewtext"><div class="write-review-reply-textarea form-control replyrev-details" valid-name="replydescription" id="write-review-reply-textare-id" contenteditable="true" data-placeholder="Write a reply. Please be constuctive. Press [Enter] to submit. Press [Shift + Enter] to Next line. And remember, your reply needs to be at least 6 characters long :) "data-par-id="' + business_id + '" data-sub-id="' + review_id + '" data-buss-name="' + business_name + '"></div></div></form></div>';

    $(this).closest('.readreviews').append(component1);
    $('div[data-rev-id="' + review_id + '"]').focus();
    $(this).parent('div').find('.fareply').hide();
    $(this).parentsUntil('.readreviews').parent('div').addClass('hasreply');
});

/* Capturing of enter key pressing in reply for review in business details page. */
$(document).on('keypress', '.replyrev-details', function (e) {

    var code = e.keyCode || e.which;
    var comp = $(this);

    if (code == 13) {
        if (!e.shiftKey) {
            e.preventDefault();
            var val;
            /* Check the reply box form Validation */
            $(this).closest("form").find('.form-control').each(function () {
                val = uservalidFunction($(this));
                if (val == 0)
                {
                    return false;
                }
            });
            if (val == 1)
            {
                /* Taking of data-sub-id for corresponding reply icon enabling */
                var rep_id = $(this).closest('.replyrev-details').attr('data-sub-id');
                /* for showing of reply icon in review*/
                $('.fa-reply[data-child-id=' + rep_id + ']').show();
                $(this).closest("form").closest('div').hide();

                form_params = {'replyrev-details': comp.text(), 'business_id': comp.attr('data-par-id'), 'review_id': comp.attr('data-sub-id'), 'business_name': comp.attr('data-buss-name')};


                /* save reply using ajax */
                $.ajax
                        ({
                            url: "/user/savereviewreply",
                            type: 'POST',
                            data: form_params,
                            success: function (response)
                            {
                                var result = eval('(' + response + ')');
                                if (result.success != true)
                                {

                                } else
                                {
                                    comp.closest('form').trigger('reset');
                                    var details = result.details;
                                    var reply_id = result.data;
                                    var user_name = result.user_name;
                                    var created_date = result.created_date;
                                    var user_id = result.user_id;
                                    var f_letter = (user_name.charAt(0));
                                    var random = (Math.floor(Math.random() * 11) + 1);
                                    // if(result.business_user)
                                    // {
                                    var inscomp1 = '<div class="mainblock clearfix readreviews repliedtext" data-reply-id="' + reply_id + '"><div class="userimage pull-left hidden-xs"><span class="firstLetterColor' + random + ' nouserphoto">' + f_letter + '</span></div><div class="reviewtext"><div class="reviewerinfo"><span class="username">' + user_name + '</span>replied <span>on ' + created_date + ' IST</span></div><div class="reviewcontent"><p>' + details + '</p></div></div></div>';
                                    // }
                                    // else
                                    // {
                                    // var inscomp1 =	'<div class="mainblock clearfix readreviews repliedtext" data-reply-id="'+reply_id+'"><div class="userimage pull-left hidden-xs"><a href="/user/reviewtable/'+user_id+'" target="_blank" title="'+user_name+'"><span class="firstLetterColor'+random+' nouserphoto">'+f_letter+'</span></a></div><div class="reviewtext"><div class="reviewerinfo"><a href="/user/reviewtable/'+user_id+'" target="_blank" title="'+user_name+'"><span class="username">'+user_name+'</span></a>replied <span>on '+created_date+' IST</span></div><div class="reviewcontent"><p>'+details+'</p></div></div></div>';
                                    // }
                                    $(inscomp1).insertBefore(comp.closest("form").closest('div'));
                                    comp.closest("form").parents(':eq(1)').find('.fareply').show();
                                }
                            }
                        });
            }
        }
    }


});


//for fancy box in business listing page for ajax contents 
$(document).ajaxComplete(function () {
    $('a.fancy_gallery').fancybox();
});

//for fancy box in business listing page for normal contents
$('a.fancy_gallery').fancybox();

/*Hidden photos click function in business details page*/
$(".hidden-photo").click(function () {
    $(".new-div").show();
});
/*bookmark function in business details page*/
$("#bookmark-id").on('click', function () {
    var bus_id = $(".business-id").val();
    if (bus_id != "")
    {
        $.ajax({
            url: '/user/bookmark',
            type: 'post',
            data: {
                'business_id': bus_id
            },
            success: function (response) {
                var res = eval('(' + response + ')');
                if (res.success == true)
                {
                    $('.btn-bookmarks').removeClass('btn-info');
                    $('.btn-bookmarks').addClass('btn-success');
                    $('.btn-bookmarks').text("Bookmarked");
                    $('.btn-bookmarks').attr({'title': 'Bookmarked', 'data-toggle': 'tooltip', 'style': 'cursor:default;background-color:#5CB85C'});
                    $('[data-toggle="tooltip"]').tooltip();
                } else
                {
                    return false;
                }
            }
        });
    }
});


/* Cancel button for reply in business details page */
$(document).on('click', '.rep_cancel', function () {

    $(this).closest("form").closest('div').remove();
    ids = $(this).attr('data-sub-id');
    $('.fa-reply[data-child-id=' + ids + ']').show();
});


/* Filter page toggle functionality */
function toggleIcon(e) {

    $(e.target)

            .prev('.box-header')

            .find(".more-less")

            .toggleClass('fa-plus fa-minus');

}

$('.box').on('hidden.bs.collapse', toggleIcon);

$('.box').on('shown.bs.collapse', toggleIcon);

$('.home-srh').on('submit', function (e) {
    $('.home-srh').attr('action', '/search/topicssearch');
    return true;

});



$(".profile-submit").on('click', function (e) {

    var formname = $(this).closest("form").attr('id');
    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    $('#' + formname).find('.error').text('');
    var val;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (val == 0)
        {
            return false;
        }
    });
    if (val === 1)
    {
        $(this).html('Sending...');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });

        $.ajax({
            url: '/user/userprofileupdate',
            data: form_params,
            type: 'POST',

            success: function (result)
            {
                var result = eval('(' + result + ')');
                console.log(result);
                if (result.success != true)
                {
                    console.log(result);
                    btn_name.html("Save");
                    btn_name.attr('disabled', false);
                    $('#' + formname).find('.error').text('');
                    $('#suc_msg').text(result.msg);
                } else
                {
                    btn_name.html("Save");
                    btn_name.attr('disabled', false);
                    $('#' + formname).find('.error').text('');
                    $('#suc_msg').text(result.msg);
                }
            }
        });

    } else
    {
        return false;
    }
});
//Modal popup trigger event in business details page.
$('.enqbutton').on('click', function (e)
{
    e.preventDefault();
    relatedbussid = $(this).attr('data-buss-id');
    $('#data_business_id').attr('value', relatedbussid);
    $('#getquote_modal').modal('show');

});
/*user review profile reply*/
$(document).on('keypress', '.user_reply_review', function (e) {

    var code = e.keyCode || e.which;
    var comp = $(this);

    if (code == 13) {

        e.preventDefault();
        var val;
        /* Check the reply box form Validation */
        $(this).closest("form").find('.form-control').each(function () {
            val = uservalidFunction($(this));
            if (val == 0)
            {
                return false;
            }
        });
        if (val == 1)
        {

            /* If the validation is true generate the form values using serialize array */
            var form_params = {};
            var form_values = $(this).closest("form").serializeArray();
            var obj = $.each(form_values, function (key, val)
            {

                if (form_params.hasOwnProperty(val.name))
                {
                    form_params[val.name] = form_params[val.name] + "," + val.value;
                } else
                {
                    if (val.value != null && val.value != " ")
                        form_params[val.name] = val.value;
                }
            });

            /* save reply using ajax */
            $.ajax
                    ({

                        url: "/user/savereviewreply",
                        type: 'POST',
                        data: form_params,
                        success: function (response)
                        {

                            var result = eval('(' + response + ')');

                            if (result.success != true)
                            {

                            } else
                            {

                                comp.closest('form').trigger('reset');
                                var details = result.details;
                                var reply_id = result.data;
                                var user_name = result.user_name;
                                var inscomp = '<div class="review_box"><div class="comment-box"><i class="fa fa-share"></i>&nbsp;&nbsp;<label>' + user_name + '</label><div class="replyrev-details" readonly name="replyrev-details" id="review-desc" valid-name="seo_desc" >' + details + '</div></div></div>';
                                $(inscomp).insertAfter(comp.closest("form"));

                            }
                        }
                    });
        }
    }
});
/*user review profile reply end*/
$('.rev_xpand').on('click', function () {
    $('.addreviewcontainer').hide();
    $('.writereviewcontainer').show();
});


$('#main-search-keyword').submit(function () {
    $(this).attr("action", "/sitebusiness/search");
    $(this).submit();
});


$("#usermodel").on('show.bs.modal', function () {
    $.getScript("https://apis.google.com/js/api:client.js", function () {
        var googleUser = {};
        var startApp = function () {
            gapi.load('auth2', function () {
                auth2 = gapi.auth2.init({
                    //client_id:'58956970460-ok7a3seu1dinip4fmhrccdmls91rk585.apps.googleusercontent.com',
                    client_id: '595778558602-mvso1ascvq3rit3ruoakp1an2j5ru2n8.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin'
                });
                attachSignin(document.getElementById('customBtn'));
            });
            function attachSignin(element) {
                auth2.attachClickHandler(element, {},
                        function (googleUser) {

                            var profile = googleUser.getBasicProfile();
                            console.log(profile);
                            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                            console.log('Name: ' + profile.getName());
                            console.log('Email: ' + profile.getEmail());
                            var social_data_param = {
                                name: profile.getName(),
                                id: profile.getId(),
                                email: profile.getEmail(),
                                gender: "",
                                link: "",
                                dob: "",
                                location: ""
                            };
                            social_login_btn(social_data_param, 1);
                        });
            }
        }
        startApp(); // gmail functionality

        $.ajaxSetup({cache: true});
        $.getScript('//connect.facebook.net/en_US/all.js', function () {
            FB.init({
                appId: '130155087616311',
                version: 'v2.10', // or v2.1, v2.2, v2.3, ...
                cookie: true, oauth: true


            });
            $('#fb_btn').removeAttr('disabled');
            //FB.getLoginStatus(checkLoginState);
        });

//Fb Api Load Default
        window.fbAsyncInit = function () {
            FB.init({
                appId: '130155087616311',
                cookie: true, // enable cookies to allow the server to access 
                //the session
                xfbml: true, // parse social plugins on this page
                version: 'v2.10' // use graph api version 2.8
            });
        };

        //Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    });
});
//For capturing the paste event of reviews in business details page.
$("#write-review-reply-textarea-id").bind("paste", function (e) {
    e.preventDefault();
    data_already = $('#write-review-reply-textarea-id').html();
    var pastedData = data_already + e.originalEvent.clipboardData.getData('text');
    $('#write-review-reply-textarea-id').html(pastedData.replace(/\r?\n/g, '<br/>'));

});

//Prevent from pasting content in business details page.
$('#writereview').bind('paste', function (e) {
    e.preventDefault();
});


//view more replies function in business details.	  
$(document).on('click', '.viewmore', function (e) {

    e.preventDefault();
    var review_id = $(this).closest('.hasreply').attr('data-review-id');
    var reply_id = $(this).parent('div').children("div[data-reply-id]:last").attr('data-reply-id');
    rep_id = $(this).parent('div').attr('data-review-id');
    comp = $(this);
    var business_id = $('#business_id').val();
    $.ajax
            ({
                url: '/user/exportreplies',
                type: 'POST',
                data: {'review_id': review_id,
                    'reply_id': reply_id,
                    'business_id': business_id},
                success: function (response)
                {
                    var res = eval('(' + response + ')');
                    if (res.success != true)
                    {
                    } else
                    {
                        rep_id = comp.parent('div').attr('data-review-id');
                        comp.nextAll('div[data-reply-id]').remove();
                        $(res.data).insertAfter(comp);
                        comp.remove();
                    }
                }
            });
});


//send to phone or mail in business details page.

$('.sendPhoneEmailbutton').on('click', function (e) {
    var formname = $(this).closest("form").attr('id');
    var err_formname = $(this).closest("form").attr('name');
    var window_form = $(this).closest("div.modal").attr('id');
    var business_id = $('#business_id').val();
    var source_url = $('#source_url').val();
    $('#' + formname).find(".err-msg").remove();

    $('#' + formname).submit(function (e) {
        e.preventDefault();
    });
    var val = 1;
    var errval = false;
    $('#' + formname).find('.form-control').each(function () {
        val = uservalidFunction($(this));
        if (!val)
            errval = true;
    });

    if (!errval)
    {
        $(this).html('Sending.......');
        $(this).attr('disabled', true);
        var btn_name = $(this);
        var form_params = {};
        var form_values = $('#' + formname).serializeArray();
        form_values.push({name: "business_id", value: business_id}, {name: "source_url", value: source_url});
        var obj = $.each(form_values, function (key, val)
        {

            if (form_params.hasOwnProperty(val.name))
            {
                form_params[val.name] = form_params[val.name] + "," + val.value;
            } else
            {
                if (val.value != null && val.value != " ")
                    form_params[val.name] = val.value;
            }
        });
        $.ajax
                ({

                    url: "/user/sendphoneormail",
                    type: 'POST',
                    data: form_params,
                    success: function (response)
                    {
                        var result = eval('(' + response + ')');
                        if (result.success != true)
                        {
                            btn_name.html("Send to Phone / Email");
                            $('.btn-save').attr('disabled', false);
                        } else
                        {
                            $("#" + window_form).modal('hide');
                            btn_name.html("Send to Phone / Email");
                            $('.btn-save').attr('disabled', false);
                            $('#' + formname).trigger("reset");
                            $('#result_modal').modal('show');
                            $('#modal_result_content').append('<i class="fa fa-bookmark-o form-group" aria-hidden="true"></i><div>We will send the details shortly.</div>');
                            setTimeout(function () {
                                $('#result_modal').modal('hide');
                                $('#modal_result_content').empty();
                            }, 5000);
                        }
                    }
                });
    } else
    {
        return false;
    }
});

//business reviews sorting by rating and published date function
$('.sortbyreviews').on('change', function ()
{
    var sort = $(this).val();
    var bus_id = $('#business_id').val();
    var name = $('#buss_name').val();

    comp = $(this);
    $.ajax({
        url: '/user/filterbusinessreviews',
        data: {
            'sorting': sort,
            'business_id': bus_id,
            'business_name': name
        },
        type: 'POST',
        success: function (result) {
            var result = eval('(' + result + ')');

            if (result.success == true) {
                comp.parent('div').parent('div').parent('div').find('.readreviews').remove();
                comp.parent('div').parent('div').parent('div').find('#more_reviews').remove();
                $('.buss-list').remove();
                comp.parent('div').parent('div').parent('div').append(result.data);
                $('#more_reviews').attr('value', sort);
            }
        }
    });
});

/*business details popup page map js*/

var source, destination;
var directionsDisplay, directionsService;
var infoWindow, geocoder, latLng;
var infonxtWindow;

function GetRoute() {
    new google.maps.places.SearchBox(document.getElementById('txtSource'));
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var dmap = new google.maps.Map(document.getElementById('dvMap'), {
        zoom: 7,
        center: latLng
    });

    directionsDisplay.setMap(dmap);

    directionsDisplay.setPanel(document.getElementById('dvPanel'));
    source = document.getElementById("txtSource").value;
    destination = document.getElementById("txtDestination").value;

    directionsService.route({
        origin: source,
        destination: destination,
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });


    //DISTANCE AND DURATION//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [source],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
            var distance = response.rows[0].elements[0].distance.text;
            var duration = response.rows[0].elements[0].duration.text;
            var dvDistance = document.getElementById("dvDistance");
            dvDistance.innerHTML = "";
            dvDistance.innerHTML += "Distance: " + distance + "<br />";
            dvDistance.innerHTML += "Duration:" + duration;

        } else {
            alert("Unable to find the distance via road.");
        }
    });

}
function initialize(lati, longti) {
    new google.maps.places.SearchBox(document.getElementById('txtSource'));
    var map = new google.maps.Map(document.getElementById('dvMap'), {
        zoom: 10,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    });
    var marker = new google.maps.Marker({
        position: latLng,
        title: 'Besant Technologies',
        map: map,
        draggable: false
    });

    var latlng = {lat: parseFloat(lati), lng: parseFloat(longti)};

    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                infoWindow.setContent(results[0].formatted_address);
            }
        }
    });
    infoWindow.open(map, marker);
}
$('#mappingmodel').on('show.bs.modal', function () {
    $.getScript("https://maps.googleapis.com/maps/api/js?sensor=true&key=AIzaSyB4aV_dstfUJhaIlDvt1eOu7d_rrInQFbw&libraries=drawing,places", function () {
        //infoWindow = new google.maps.InfoWindow();
        infoWindow = new google.maps.InfoWindow({
            size: new google.maps.Size(150, 50)
        });
        infonxtWindow = new google.maps.InfoWindow({
            size: new google.maps.Size(150, 50)
        });

        new google.maps.places.SearchBox(document.getElementById('txtSource'));
        geocoder = new google.maps.Geocoder();
        directionsService = new google.maps.DirectionsService();

        lat = $('.modelmap').attr('data-lat');
        longt = $('.modelmap').attr('data-lang');
        latLng = new google.maps.LatLng(lat, longt);
        initialize(lat, longt);
    });
});
$('#mappingmodel').on('show.bs.modal', function () {

    $('#dvPanel').empty();
    $('#dvDistance').empty();
    $('#txtSource').val('');
});

$(document).on('click', '#more_reviews', function (e) {
    e.preventDefault();
    var name = $('#buss_name').val();
    var buss_id = $('#business_id').val();
    var rev_id = $(this).prev('div').attr('data-review-id');
    var sort = $(this).attr('value');
    var limit = $(this).attr('data-limit');
    comp = $(this);
    $.ajax({
        url: '/user/more_review',
        data: {'rev_id': rev_id,
            'business_id': buss_id,
            'name': name,
            'sorting': sort,
            'limit': limit
        },
        type: 'POST',
        success: function (response)
        {
            var result = eval('(' + response + ')');
            if (result.success == true)
            {
                comp.parent('div').append(result.data);
                comp.remove();
            } else
            {
            }
        }

    })


});
/*businessdetails map pop up ends 
 business form mapping*/
if ($('div').is('#mapCanvas')) {
    var newnxtinfoWindow;
    var newinfowindow;
    var newgeocoder;
    $.getScript("https://maps.googleapis.com/maps/api/js?sensor=true&key=AIzaSyB4aV_dstfUJhaIlDvt1eOu7d_rrInQFbw&libraries=drawing,places", function () {
        function mapinitialize() {
            newnxtinfoWindow = new google.maps.InfoWindow();
            newinfowindow = new google.maps.InfoWindow({
                size: new google.maps.Size(150, 50)
            });
            newgeocoder = new google.maps.Geocoder();
            function mapgeocodePosition(pos) {
                newgeocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        newnxtinfoWindow.setContent(responses[0].formatted_address);

                        mapupdateMarkerAddress(responses[0].formatted_address);
                    } else {
                        mapupdateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }
            function mapupdateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function mapupdateMarkerPosition(latLng) {
                document.getElementById('info_new').innerHTML = [
                    latLng.lat(),
                    latLng.lng()
                ].join(', ');
            }

            function mapupdateMarkerAddress(str) {
                document.getElementById('address').innerHTML = str;
            }



            var latLng = new google.maps.LatLng(12.925131, 80.230416);
            var maps = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 5,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var newmarker = new google.maps.Marker({
                position: latLng,
                title: 'Point A',
                map: maps,
                draggable: true
            });
            // Update current position info.
            mapupdateMarkerPosition(latLng);
            mapgeocodePosition(latLng);
            // Add dragging event listeners.
            google.maps.event.addListener(newmarker, 'dragstart', function () {
                mapupdateMarkerAddress('Dragging...');
            });
            google.maps.event.addListener(newmarker, 'drag', function () {
                mapupdateMarkerStatus('Dragging...');
                mapupdateMarkerPosition(newmarker.getPosition());
            });
            google.maps.event.addListener(newmarker, 'dragend', function () {
                mapupdateMarkerStatus('Drag ended');
                mapgeocodePosition(newmarker.getPosition());
            });
            google.maps.event.addListener(newmarker, 'click', function () {
                newinfoWindow.open(maps, newmarker);
            });

        }

        google.maps.event.addDomListener(window, 'load', mapinitialize);
        newinfoWindow.open(maps, newmarker);

    });
}

/*business form mapping ends*/
/* Business Details Page Share Button Start */

$('.facebook_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.twitter_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'twitterShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.google_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'googleShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.linkedin_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'linkedinShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.pinterest_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'pinterestShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.stumbleupon_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'stumbleuponShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=3');
    return false;
});
$('.digg_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'diggShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.reddit_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'redditShareWindow', 'height=450, width=750, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});
$('.tumblr_com').click(function (e) {
    e.preventDefault();
    window.open($(this).attr('href'), 'tumblrShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=2');
    return false;
});

/* Business Details Page share Button End */



